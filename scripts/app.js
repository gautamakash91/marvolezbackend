var XLSX = require('xlsx');
var fs = require('fs');
var workbook = XLSX.readFile('restaurant-fixed.xlsx');
var sheet_name_list = workbook.SheetNames;
var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

function replaceAll(string, search, replacement) {
    var target = string;
    return target.split(search).join(replacement);
};

var records = [];
var  restaurants = [];

for(var i = 0; i < xlData.length; i++){
	// console.log(xlData[i]);
	// console.log("----------------");
	// console.log(xlData[i].Price);
	if(xlData[i].hasOwnProperty("Price")){
		var splitData = xlData[i].Price.split("|");
		// console.log(splitData.length);
		// console.log("****************");
		if(splitData.length > 1){
			for(var j = 0; j < splitData.length; j++){
				splitData[j] = replaceAll(splitData[j],"$","");
				splitData[j] = replaceAll(splitData[j]," ","");
			}
			xlData[i].Price = splitData;
		}
	}
	//console.log(xlData[i]);
	records.push(xlData[i]);
}

console.log('excel read done');

var rcount = -1;
var finalRecords = [];
var mainCount = 0;
for(var i = 0; i < records.length; i++){
	if(records[i].hasOwnProperty("Restaurant")){
		if(!isInArray(restaurants, records[i].Restaurant)){
			++rcount;
			var Obj = {};
			restaurants[rcount] = records[i].Restaurant;
			Obj.Restaurant = restaurants[rcount];
			Obj.Menu = [];
			for(var j = 0; j < records.length; j++){
				if(restaurants[rcount] == records[j].Restaurant){
					var key = "Restaurant";
					var menu = records[j];
					delete menu[key];
					Obj.Menu.push(menu);
					mainCount++;
				}
			}
			finalRecords.push(Obj);
		}
	}
}
console.log("restaurants count :"+rcount);
console.log('main count : '+mainCount);

fs.writeFile("data.json", JSON.stringify(finalRecords), function(err) {
    if(err) {
        return console.log(err);
    }
    else
		console.log("The file was saved!");
});


function mongoInsert(myobj){
	var MongoClient = require('mongodb').MongoClient;
	//var url = "mongodb://localhost:27017/";
	//DEV
	//var url = "mongodb://stellosphere:vision2020@marvolez-shard-00-00-fch1s.mongodb.net:27017,marvolez-shard-00-01-fch1s.mongodb.net:27017,marvolez-shard-00-02-fch1s.mongodb.net:27017/test?ssl=true&replicaSet=Marvolez-shard-0&authSource=admin";
	//PROD
	var url ="mongodb://stellosphere:vision2020@cluster0-shard-00-00-zxb0w.mongodb.net:27017,cluster0-shard-00-01-zxb0w.mongodb.net:27017,cluster0-shard-00-02-zxb0w.mongodb.net:27017/Marvolz?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true"  

	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var dbo = db.db("Marvolz");
	  dbo.collection("menus").insertMany(myobj, function(err, res) {
	    if (err) throw err;
	    console.log("Number of documents inserted: " + res.insertedCount);
	    db.close();
	  });
	});
}

function isInArray(arr, item){
	if(arr.indexOf(item) != -1){  
	   return true;
	}else{
		return false;
	}
}

mongoInsert(finalRecords);
