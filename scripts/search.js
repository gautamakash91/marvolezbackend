var MongoClient = require('mongodb').MongoClient;
//var url = "mongodb://35.227.103.170:27017/";
//DEV
//var url = "mongodb://stellosphere:vision2020@marvolez-shard-00-00-fch1s.mongodb.net:27017,marvolez-shard-00-01-fch1s.mongodb.net:27017,marvolez-shard-00-02-fch1s.mongodb.net:27017/test?ssl=true&replicaSet=Marvolez-shard-0&authSource=admin";
//PROD
var url ="mongodb://stellosphere:vision2020@cluster0-shard-00-00-zxb0w.mongodb.net:27017,cluster0-shard-00-01-zxb0w.mongodb.net:27017,cluster0-shard-00-02-zxb0w.mongodb.net:27017/Marvolz?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true"  

var async = require('async');
var resultArry = [];
MongoClient.connect(url, function(err, db){
	var dbo = db.db("Marvolz");
    var cursor = dbo.collection('menus').find();
    cursor.forEach(function(doc, err){
    	var obj = {};
    	if(doc.hasOwnProperty("Restaurant"))
    	obj.Name = doc.Restaurant.toLowerCase();
    	obj._id = doc._id;
      	resultArry.push(obj);
    },
    function(){
      db.close();
      sendControlForMenuId(resultArry);
    });
});

function sendControlForMenuId(arr){
	var resultArry = [];
	MongoClient.connect(url, function(err, db){
		var dbo = db.db("Marvolz");

		async.forEachSeries(arr, function(item, callback) {
			console.log('Restaurant Name '+ item.Name);
			var myquery = {"Name" : item.Name};
			var newvalues = { $set: { menuid: item._id } };
			dbo.collection("Restaurants").updateMany(myquery, newvalues, function(err, res) {
				if (err) throw err;
				console.log(res.result.nModified + " document(s) updated");
				callback();
			});
		}, function(){
			db.close();
			console.log('done iterating');
		});
	});
}