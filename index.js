
var express = require('express');
var app = express();
var mongo = require('mongodb');
var assert = require('assert');
var bodyParser = require('body-parser');
var cors = require('cors');
var plivo = require('plivo');
var ObjectID = require('mongodb').ObjectID; 
var geodist = require('geodist');
var nodemailer = require('nodemailer');
var fs = require('fs');
var Hogan=require('hogan.js');


var clientroute = require('./routes/clientroute');
var restaurantroute = require('./routes/restaurantroute');
var marvolezroute = require('./routes/marvolezroute');
var userroute = require('./routes/userroute');
var paymentroute = require('./routes/paymentroute');
var admin = require ("firebase-admin");
var serviceAccount = require ("./admin2.json");

//Plivo credentials
var p = plivo.RestAPI({
	authId: 'MAYTE4Y2U5MDM3ZWU5YJ',
	authToken: 'OTIxY2ZjMDU2M2FhYmRmMTcwMTYzNDM1ZDI3YjA1'
});

// Twilio Credentials
const accountSid = 'ACbcfc229c78458b43028d64323d41336b';
const authToken = 'f7db91643a7f4f950b7d59b55ccd596d';

// require the Twilio module and create a REST client
const client = require('twilio')(accountSid, authToken);

app.set('port', (process.env.PORT || 8000));


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//OLD
//var url = "mongodb://stellosphere:vision2020@marvolez-shard-00-00-fch1s.mongodb.net:27017,marvolez-shard-00-01-fch1s.mongodb.net:27017,marvolez-shard-00-02-fch1s.mongodb.net:27017/test?ssl=true&replicaSet=Marvolez-shard-0&authSource=admin";

//PROD
var url ="mongodb://stellosphere:vision2020@cluster0-shard-00-00-zxb0w.mongodb.net:27017,cluster0-shard-00-01-zxb0w.mongodb.net:27017,cluster0-shard-00-02-zxb0w.mongodb.net:27017/Marvolz?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true"  

//local
//var url = "mongodb://35.227.103.170:27017/marvolez";
//var url = "mongodb://localhost:27017/marvolez";
//new route for clientApp, restaurants and marvolez

clientroute.configure(app, mongo, ObjectID, url, assert);
restaurantroute.configure(app, mongo, url, assert, ObjectID, geodist);
marvolezroute.configure(app, mongo, url, assert, ObjectID);
userroute.configure(app, mongo, ObjectID, url, assert);
paymentroute.configure(app, mongo, url, assert, ObjectID);

//Sample API377
app.get('/', function(req, res){
	res.send("WELCOME TO MARVOLEZ API'S");
});


//EMAIL SENT WITH TEMPLATES

var template=fs.readFileSync('view/image2.hjs','utf-8');
var compiledTemplate=Hogan.compile(template);

app.post('/email2',function (req, res) { 
  var transporter = nodemailer.createTransport({
 service: 'gmail',
 auth: {
        user: 'soumya9778465@gmail.com',
        pass: 'diploma68'
    }
});
var mailOptions = {
  from: 'soumya9778465@gmail.com', // sender address
  to: req.body.email, // list of receivers
  subject: 'You Are Successfully registered', // Subject line
  html: compiledTemplate.render({})// plain text body
};
transporter.sendMail(mailOptions, function (err, info) {
   if(err){
     console.log(err);
      res.json({ status: false });
   }

   else{
     console.log(info);
     res.json({ status: true });
   }
});
});





//API FOR UPDATE USER DETAILS
app.post('/updateUsers', function (req, res) {
	try {
		if (req.body.hasOwnProperty("id")&&req.body.hasOwnProperty("email")&&req.body.hasOwnProperty("name")&&req.body.hasOwnProperty("phone")&&req.body.hasOwnProperty("login_date")) {

			mongo.connect(url, function (err, db) {
				var dat = require('date-and-time');
				var date = dat.format(new Date(), 'DD-MM-YYYY');
				assert.equal(null, err);
				db.collection('USER').findOneAndUpdate({ "_id": new ObjectID(req.body.id) }, {
					$set: {

						login_date: date,
						email:req.body.email,
						name:req.body.name,
						phone:req.body.phone,
					}
				},

				{ upsert: true }, function (err, result) {

					if (err) {
						res.json({ status: false });
					} else {

						res.json({ status: true });

					}
					db.close();

				});
			});

		}
		else {
			console.log("some empty field");
			res.json({ status: false, Message: "Some fields are Missing" });
		}
	}
	catch (er) {
		console.log("error occured : " + er);
		res.json({ status: false, Message: er });
	}

});

//API FOR PUSHER NOTIFICATION
app.post('/pusher', function(req, res){

	try {


		admin.initializeApp({ credential: admin.credential.cert(serviceAccount),
			databaseURL: "https://marvolez-db151.firebaseio.com"
  //databaseURL: "https://fcm.googleapis.com/fcm/send"
});
//var push = "dU5ZtKQSDUQ:APA91bH_Bfb598zdeIhSYQgkTi_inhU5B-u6Mvlkm27ZVmsEUeYBU-YROnzjW58M5aP77SuxRNKRy3E-8mMiRPjcJPY4em_ljLhcq6vfgaSqAz0odvh3_OWEmMMj5y7suuu4Ggv0LrCT";
var registrationToken = req.body.push;

var payload = {
	notification: {
		title: 'MARVOLEZ',
		body: 'This is marvolez'
	}
};
var options ={

	priority:"high",
  // timeToLive:60*60*24
};

admin.messaging().sendToDevice(registrationToken,payload,options)
.then(function(response){
	console.log("successfully sent message:",response);
	res.json({status:true,message:"successfully sent message"});

})
.catch(function(error){
	console.log("Error sending message:",error);
	res.json({status:false,message:"Error sending message"})
})
}


catch (er) {
	console.log("error occured : " + er);
	res.json({ status: false, Message: er });
}


});

  //API for adding new restaurants
  //tested on 26th sept 2017: Akash Gautam

  app.post('/addRestaurant', function(req, res){
  	if(req.body.hexCode=="TiKfGRskuIMB2h7iFOyszpCE3Sxpgp"){
  		var newRestaurant = {
        //id: req.body.id,
        name: req.body.name,
        address: req.body.address,
        contactPerson: req.body.contactPerson,
        city: req.body.city,
        country: req.body.country,
        phone: req.body.phone,
        pincode:req.body.pincode,
        email: req.body.email,
        location:[req.body.lat,req.body.long]
    };

    mongo.connect(url, function(err, db){
    	assert.equal(null, err);
    	db.collection('Restaurants').insertOne(newRestaurant,function(err, result){
    		if(err){
    			res.send("false");
    		}else{
    			res.send("true");
    		}
    		db.close();
    	});
    });
}
});


//API TO GET RESTAURANTS BASED ON SEARCH CRITERIA ON ADMIN PORTAL
//tested 26th September 2017: Akash Gautam
app.post('/getByInfo', function(req, res){
	console.log(req.body.name+" "+req.body.pincode);
	var resultArry = [];
	mongo.connect(url, function(err, db){
		assert.equal(null, err);
		if(req.body.city == "" && req.body.name == "") {
      //var cursor = db.collection('Restaurants').find();
  } else if(req.body.city == "") {
  	var cursor = db.collection('Restaurants').find({ "Name":req.body.name });
  } else if(req.body.name == "") {
  	var cursor = db.collection('Restaurants').find({ "Pincode": parseInt(req.body.pincode) });
  }else{
  	var cursor = db.collection('Restaurants').find({ "Name":req.body.name , "Pincode": parseInt(req.body.pincode) });
  }

  cursor.forEach(function(doc, err){
  	assert.equal(null, err);
  	resultArry.push(doc);
  }, function(){
  	db.close();
  	res.json(resultArry);
  });
});
});




//list of all restaurant of a owner returned by using owners Email
app.post('/getByEmail', function(req, res){
	var resultArry = [];
	mongo.connect(url, function(err, db){
		assert.equal(null, err);
		var cursor = db.collection('Restaurants').find(
			{ "Owner":req.body.email}
			);
		cursor.forEach(function(doc, err){
			assert.equal(null, err);
			resultArry.push(doc);
		},
		function(){
			db.close();
			res.json(resultArry);
		});
	});

});

app.post('/payout_amount', function(request, response) { 
  response.send({
    value: '2',
});
})

//API for claim restaurant in search page

app.post('/claimrestaurant', function(req, res){

	var addClaim = {

	};

	mongo.connect(url, function(err, db){
		assert.equal(null, err);
		db.collection('Restaurants').insertOne(addClaim,function(err, result){
			if(err){
				res.send("false");
			}else{
				res.send("true");
			}
			db.close();
		});
	});
});

app.post('/plivo', function(request, response) {
	var code = request.body.code;
	var params = {
    'to': request.body.phone,    // The phone numer to which the call will be placed
    'from' : '+16318974678', // The phone number to be used as the caller id

    // answer_url is the URL invoked by Plivo when the outbound call is answered
    // and contains instructions telling Plivo what to do with the call
    'answer_url' : "http://dev-env.vhfk2cqrrc.us-east-1.elasticbeanstalk.com/plivoanswered/" + encodeURIComponent(code),
    'answer_method' : "POST", // The method used to call the answer_url
};
p.make_call(params, function (status, response) {
	console.log('Status: ', status);
	console.log('API Response:\n', response);
});
response.send("call made");
});

app.post('/plivoanswered/:code', function(request, response) {
	var r = plivo.Response();
	var body;
	body = "Hello, your authentication code is "+request.params.code;
	var params1 = {
		'language': "en-US", 
		'voice': "WOMAN",
		'loop':"true"
	};
	r.addSpeak(body, params1);
	response.set({'Content-Type': 'text/xml'});
	response.send(r.toXML());
});


app.listen(app.get('port'), function() {
	console.log('Node app is running on port', app.get('port'));
});
