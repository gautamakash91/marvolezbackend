module.exports = function(mongo, url, assert){

	var restaurantApp = {

		dbName : 'menus',

		restaurantDB : 'Restaurants',

		unverifiedRestaurantsDB : 'unverified_restaurants',

		getMenu : function(menuid, callBack){
			try{
				var self = this;
				var resultArry = [];
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    var cursor = db.collection(self.dbName).find({"_id": menuid});
				    cursor.forEach(function(doc, err){
				      assert.equal(null, err);
				      resultArry.push(doc);
				    },
				    function(){
				      db.close();
				      if(resultArry.length){
				      	callBack(null, resultArry);
				      }else{
				      	callBack(true, "nomenu");
				      }
				    });

				  });
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		claimedSingleRestaurant: function(ownerHash, restaurant_id, callBack){
			try{
				var self = this;
				var resultArry = [];
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    var cursor = db.collection(self.restaurantDB).find({"ownerHash": ownerHash, "_id": restaurant_id});
				    cursor.forEach(function(doc, err){
				      assert.equal(null, err);
				      resultArry.push(doc);
				    },
				    function(){
				      db.close();
				      if(resultArry.length){
				      	callBack(null, resultArry);
				      }else{
				      	callBack(true, "norestaurant");
				      }
				    });

				  });
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		claimedRestaurant : function(ownerHash, callBack){
			try{
				var self = this;
				var resultArry = [];
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    var cursor = db.collection(self.restaurantDB).find({"ownerHash": ownerHash});
				    cursor.forEach(function(doc, err){
				      assert.equal(null, err);
				      resultArry.push(doc);
				    },
				    function(){
				      db.close();
				      if(resultArry.length){
				      	callBack(null, resultArry);
				      }else{
				      	callBack(true, "norestaurant");
				      }
				    });

				  });
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		getAdSharing : function(restaurant_id, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					db.collection(self.restaurantDB).findOne({"_id": restaurant_id}, function(er, result) {
						db.close();
						if (er){
							callBack(true, null);
						}else{
							if(result == null){
								callBack(null, false);
							}else if(result.hasOwnProperty("adSharing")){
								callBack(null, result.adSharing);
							}else{
								callBack(null, false);
							}
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		updateAdSharing: function(details, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					var newvalues = { $set: {adSharing: details.adSharing } };
					db.collection(self.restaurantDB).updateOne({"_id": details._id}, newvalues, function(err, result){
						db.close();
						if (err){
							callBack(true, null);
						}else{
							callBack(null, true);
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		getUnverifiedRestaurant : function(restaurant_id, callBack){
			try{
				var self = this;
				var resultArry = [];
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    var cursor = db.collection(self.unverifiedRestaurantsDB).find({"_id": restaurant_id});
				    cursor.forEach(function(doc, err){
				      assert.equal(null, err);
				      resultArry.push(doc);
				    },
				    function(){
				      db.close();
				      if(resultArry.length){
				      	callBack(null, resultArry);
				      }else{
				      	callBack(true, "norestaurant");
				      }
				    });
				  });
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		addOwner : function(details, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					var newvalues = { $set: {ownerHash: details.ownerHash} };
					db.collection(self.restaurantDB).updateOne({"_id": details._id}, newvalues, function(err, result){
						db.close();
						if (err){
							callBack(true, null);
						}else{
							callBack(null, true);
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		addUnverified : function(details, callBack, verified = true){
			try{
				var self = this;
				var targetDB = self.unverifiedRestaurantsDB;
				if(verified){
					targetDB = self.restaurantDB;
				}
				mongo.connect(url, function(err, db){
					assert.equal(null, err);
					db.collection(targetDB).insertOne(details,function(err, result){
						db.close();
						if(err){
							callBack(true, null);
						}else{
							callBack(null, true);
						}
					});
		        });
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		verifyRest : function(restaurant_id, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db){
					assert.equal(null, err);
					var cursor = db.collection(self.unverifiedRestaurantsDB).find({"_id": restaurant_id});
					cursor.forEach(function(doc, err){
						assert.equal(null, err);
						console.log(doc);
						db.collection(self.restaurantDB).insertOne(doc,function(err, result){
							console.log(err);
							if(err){
								callBack(true, null);
							}else{
								callBack(null, true);
							}
						});
					  },
					  function(){
						db.close();
						callBack(true, "no restaurant");
					  });
		        });
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		}

	}
	return restaurantApp;
}