module.exports = function(mongo, url, assert){

	var userApp = {

		dbName : 'UserDetails',
		dbName2 : 'User_Sessions',
		addUser : function(details, callBack) {
			try{
				var self = this;
				mongo.connect(url, function(err, db){
					assert.equal(null, err);
					db.collection(self.dbName).insertOne(details, function(err, result){
						db.close();
						if(err){
							callBack(true, null);
						}else{
							callBack(null, true);
						}
					});
				});
			}catch(e){
				console.log("error : "+e);
				callBack(true, null);
			}
		},


		Userinstalled : function(details, callBack) {
			try{
				var self = this;
				mongo.connect(url, function(err, db){
					assert.equal(null, err);
					db.collection(self.dbName).insertOne(details, function(err, result){
						db.close();
						if(err){
							callBack(true, null);
						}else{
							callBack(null, result);

						}
					});
				});
			}catch(e){
				console.log("error : "+e);
				callBack(true, null);
			}
		},

		Useropend : function(details, callBack) {
			try{
				var self = this;
				mongo.connect(url, function(err, db){
					assert.equal(null, err);
					db.collection(self.dbName2).insertOne(details, function(err, result){
						db.close();
						if(err){
							callBack(true, null);
						}else{
							callBack(null, result);

						}
					});
				});
			}catch(e){
				console.log("error : "+e);
				callBack(true, null);
			}
		},


		Userupdate : function(details, callBack) {
			try{
				var self = this;
				mongo.connect(url, function(err, db){
					var dat = require('date-and-time');
					var date = dat.format(new Date(), 'DD-MM-YYYY');
					assert.equal(null, err);
					var newvalues = { $set: 
						{
							login_date: date,
							email:req.body.email,
							name:req.body.name,
							phone:req.body.phone,} };

							db.collection(self.dbName).updateOne({"_id": details.id,},newvalues,function(err, result){
								db.close();
								if(err){
									callBack(true, null);
								}else{
									callBack(null, result);

								}
							})
						});
			}catch(e){
				console.log("error : "+e);
				callBack(true, null);
			}
		},



	// updatePassword : function(details, callBack){
	// 			try{
	// 				var self = this;
	// 				mongo.connect(url, function(err, db) {
	// 					assert.equal(null, err);
	// 					var newvalues = { $set: {password: details.password } };
	// 					db.collection(self.dbName).updateOne({"email": details.email}, newvalues, function(err, result){
	// 						db.close();
	// 						if (err){
	// 							callBack(true, null);
	// 						}else{
	// 							callBack(null, true);
	// 						}
	// 					});
	// 				});
	// 			}catch(e){
	// 				console.log("error occured : "+e);
	// 				callBack(true, null);
	// 			}
	// 		},

















	connectFacebook : function(id, FacebookFriends, callBack){
		try{
			var self = this;
			mongo.connect(url, function(err, db){
				assert.equal(null, err);
				db.collection(self.dbName).updateOne({"_id": id}, { $set: {FacebookFriends: parseInt(FacebookFriends), isFacebookConnected: true} }, {upsert: true}, function(err, result){
					db.close();
					if (err){
						callBack(true, null);
					}else{
						callBack(null, true);
					}
				});
			});
		}catch(e){
			console.log("error occured : "+e);
			callBack(true, null);
		}
	},

	connectTwitter : function(id, TwitterFriends, callBack){
		try{
			var self = this;
			mongo.connect(url, function(err, db){
				assert.equal(null, err);
				db.collection(self.dbName).updateOne({"_id": id}, { $set: {TwitterFriends: parseInt(TwitterFriends), isTwitterConnected: true} }, {upsert: true}, function(err, result){
					db.close();
					if (err){
						callBack(true, null);
					}else{
						callBack(null, true);
					}
				});
			});
		}catch(e){
			console.log("error occured : "+e);
			callBack(true, null);
		}
	},

	isUserExist : function(details, callBack){
		try{
			var self = this;
			var resultArry = [];
			mongo.connect(url, function(err, db){
				assert.equal(null, err);
				db.collection(self.dbName).findOne({"email": details.email}, function(er, result) {
					db.close();
					if (er){
						callBack(true, null);
					}else{
						if(result == null){
							callBack(null, 0);
						}else if(result.hasOwnProperty("email")){
							callBack(null, details);
						}
					}
				});
			});
		}catch(e){
			console.log("error occured : "+e);
			callBack(true, null);
		}
	},

	getUserDetails : function(details, callBack){
		try{
			var self = this;
			var resultArry = [];
			mongo.connect(url, function(err, db){
				assert.equal(null, err);
				db.collection(self.dbName).findOne({"_id": details._id}, function(er, result) {
					db.close();
					if (er){
						callBack(true, null);
					}else{
						if(result == null){
							callBack(null, 0);
						}else if(result.hasOwnProperty("email")){
							callBack(null, result);
						}
					}
				});
			});
		}catch(e){
			console.log("error occured : "+e);
			callBack(true, null);
		}
	},

	login : function(details, callBack){
		try{
			var self = this;
			var resultArry = [];
			mongo.connect(url, function(err, db){
				assert.equal(null, err);
				var cursor = db.collection(self.dbName).find({email: details.email});
				cursor.forEach(function(doc, err){
					assert.equal(null, err);
					if(doc.password == details.password){
						if(doc.isTwitterConnected && doc.isFacebookConnected){
							resultArry.push({valid:"true", token : doc, isFacebookConnected: doc.isFacebookConnected, isTwitterConnected:doc.isTwitterConnected, TwitterFriends: doc.TwitterFriends, FacebookFriends: doc.FacebookFriends});
						}else if(doc.isTwitterConnected){
							resultArry.push({valid:"true", token : doc,isTwitterConnected:doc.isTwitterConnected, isFacebookConnected:false, TwitterFriends: doc.TwitterFriends});
						}else if(doc.isTwitterConnected){
							resultArry.push({valid:"true", token : doc,isFacebookConnected:doc.isFacebookConnected, isTwitterConnected:false, FacebookFriends: doc.FacebookFriends});
						}else{
							resultArry.push({valid:"true", token : doc,isFacebookConnected:false, isTwitterConnected:false});
						}
					}else{
						resultArry.push({valid:"false"});
					}
				}, function(){
					db.close();
					callBack(null, resultArry);
				});
			});
		}catch(e){
			console.log("error occured : "+e);
			callBack(true, null);
		}
	},

	updateUserCredits : function(details, callBack){
		try{
			var self = this;
			mongo.connect(url, function(err, db) {
				assert.equal(null, err);
				var newvalues = { $set: {add_credits: details.add_credits } };
				db.collection(self.dbName).updateOne({"_id": details._id}, newvalues, function(err, result){
					db.close();
					if (err){
						callBack(true, null);
					}else{
						callBack(null, true);
					}
				});
			});
		}catch(e){
			console.log("error occured : "+e);
			callBack(true, null);
		}
	},

	updatePassword : function(details, callBack){
		try{
			var self = this;
			mongo.connect(url, function(err, db) {
				assert.equal(null, err);
				var newvalues = { $set: {password: details.password } };
				db.collection(self.dbName).updateOne({"email": details.email}, newvalues, function(err, result){
					db.close();
					if (err){
						callBack(true, null);
					}else{
						callBack(null, true);
					}
				});
			});
		}catch(e){
			console.log("error occured : "+e);
			callBack(true, null);
		}
	},

}
return userApp;
}