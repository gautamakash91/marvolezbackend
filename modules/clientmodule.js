module.exports = function(mongo, url, assert){

	var clientApp = {

		dbName : 'ClientDetails',
		restaurantDB : 'Restaurants',

		registerClient : function(details, callBack) {
			try{
				var self = this;
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    db.collection(self.dbName).insertOne(details, function(err, result){
				    	db.close();
				    	if(err){
				   			callBack(true, null);
				    	}else{
				    		callBack(null, true);
				    	}
				    });
				});
			}catch(e){
				console.log("error : "+e);
				callBack(true, null);
			}
		},
		
		clientLogin : function(details, callBack){
			try{
				var self = this;
				var resultArry = [];
				resultArry.push({valid:"false"});
			  	mongo.connect(url, function(err, db){
			    	assert.equal(null, err);
			    	var cursor = db.collection(self.dbName).find({email: details.email});
				    cursor.forEach(function(doc, err){
						assert.equal(null, err);
						if(doc.password == details.password){
							resultArry = [];
							resultArry.push({valid:"true",type:doc.type, token : doc});
						}else{
							resultArry.push({valid:"false"});
						}
				    }, function(){
						db.close();
						if(resultArry[0].valid == "false"){
							callBack(true, null);
						}else{
							callBack(null, resultArry);
						}
				    });
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		addSpecial : function(RestaurantId, Special, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					var newvalues = { $set: {Specials: Special } };
					db.collection(self.restaurantDB).updateOne({"_id": RestaurantId}, newvalues, {upsert: false}, function(err, result){
						db.close();
						if (err){
							callBack(true, null);
						}else{
							callBack(null, true);
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		updatePassword : function(details, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					var newvalues = { $set: {password: details.password } };
					db.collection(self.dbName).updateOne({"email": details.email}, newvalues, function(err, result){
						db.close();
						if (err){
							callBack(true, null);
						}else{
							callBack(null, true);
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		ifClientExists : function(email, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					db.collection(self.dbName).findOne({"email": email}, function(er, result) {
						db.close();
						if (er){
							callBack(true, null);
						}else{
							if(result == null){
								callBack(true, 0);
							}else{
								callBack(null, result);
							}
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		}

	}
	return clientApp;
}