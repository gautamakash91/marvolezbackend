module.exports = function(mongo, url, assert){

	var marvolezApp = {

		marvelsDB : 'marvels_transactions',

		creditsTransactionDB : 'credits_transactions',

		marvolezCreditsTransactionDB : 'marvolez_credits_transactions',

		claimTransactionsDB : 'claim_transactions',

		olezDB : 'olez',

		marvolezCreditsDB : 'marvolez_credits',

		olezTransaction : 'olez_transactions',

		restaurantDB : 'Restaurants',

		dealsDB : 'Deals',

		redemptionDB : 'Redemptions',

		getPastTransactions : function(details, callBack, credited = false){
			try{
				var self = this;
				var resultArry = [];
				mongo.connect(url, function(err, db){
					assert.equal(null, err);
					console.log(details);
				    var cursor = db.collection(self.claimTransactionsDB).find({"user_id": details.user_id, "restaurant_id": details.restaurant_id, "status" : "claimed"});
				    cursor.forEach(function(doc, err){
				      assert.equal(null, err);
				      resultArry.push({isFacebook:doc.isFacebook, isTwitter:doc.isTwitter});
				    },
				    function(){
					  db.close();
					  
				      if(resultArry.length){
				      	console.log(resultArry)
				      	callBack(null, resultArry);
				      }else{
				      	callBack(true, "notransactions");
				      }
				    });

				  });
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		getUserDetails : function(details, callBack){
			try{
				var self = this;
				var resultArry = [];
			  	mongo.connect(url, function(err, db){
			    	assert.equal(null, err);
			    	db.collection("UserDetails").findOne({"_id": details._id}, function(er, result) {
						db.close();
						if (er){
							callBack(true, null);
						}else{
							if(result == null){
								callBack(null, 0);
							}else if(result.hasOwnProperty("email")){
								delete result.password;
								callBack(null, result);
							}
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		transactDeal : function(details, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					var newvalues = { $set: {isActive: details.isActive, last_updated : details.updationTime } };
					db.collection(self.dealsDB).updateOne({"_id": details.deal_id}, newvalues, function(err, result){
						db.close();
						if (err){
							callBack(true, null);
						}else{
							callBack(null, true);
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		transaction : function(details, callBack, deals = false){
			try{
				var self = this;
				var dbName = self.marvelsDB;
				if(deals == true)
					dbName = self.dealsDB;
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    db.collection(dbName).insertOne(details, function(err, result){
				    	db.close();
				    	if(err){
				   			callBack(true, null);
				    	}else{
				    		if(deals == true){
				    			callBack(null, result.ops[0]._id);
				    		}else{
					    		callBack(null, true);
					    	}
				    	}
				    });
				});
			}catch(e){
				console.log("error : "+e);
				callBack(true, null);
			}
		},

		claimTransaction : function(details, callBack){
			try{
				var self = this;
				var dbName = self.claimTransactionsDB;
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    db.collection(dbName).insertOne(details, function(err, result){
				    	db.close();
				    	if(err){
				   			callBack(true, null);
				    	}else{
				    		callBack(null, true);
				    	}
				    });
				});
			}catch(e){
				console.log("error : "+e);
				callBack(true, null);
			}
		},

		getClaimTransactions : function(restaurantId, callBack){
			try{
				var self = this;
				var resultArry = [];
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    var cursor = db.collection(self.claimTransactionsDB).find({"restaurant_id": restaurantId});
				    cursor.forEach(function(doc, err){
				      assert.equal(null, err);
				      resultArry.push(doc);
				    },
				    function(){
				      db.close();
				      if(resultArry.length){
				      	console.log(resultArry)
				      	callBack(null, resultArry);
				      }else{
				      	callBack(true, "notransactions");
				      }
				    });

				  });
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		getAllPostTransactions : function(userid, callBack, credited = false){
			try{
				var self = this;
				var resultArry = [];
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    var cursor = db.collection(self.claimTransactionsDB).find({"user_id": userid});
				    if(credited){
				    	cursor = db.collection(self.claimTransactionsDB).find({"user_id": userid, "status" : "claimed"});
				    }
				    cursor.forEach(function(doc, err){
				      assert.equal(null, err);
				      resultArry.push(doc);
				    },
				    function(){
				      db.close();
				      if(resultArry.length){
				      	console.log(resultArry)
				      	callBack(null, resultArry);
				      }else{
				      	callBack(true, "notransactions");
				      }
				    });

				  });
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		creditTransaction : function(details, callBack, deals = false){
			try{
				var self = this;
				var dbName = self.creditsTransactionDB;
				if(deals == true)
					dbName = self.dealsDB;
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    db.collection(dbName).insertOne(details, function(err, result){
				    	db.close();
				    	if(err){
				   			callBack(true, null);
				    	}else{
				    		if(deals == true){
				    			callBack(null, result.ops[0]._id);
				    		}else{
					    		callBack(null, true);
					    	}
				    	}
				    });
				});
			}catch(e){
				console.log("error : "+e);
				callBack(true, null);
			}
		},

		getRestaurantsTransactions : function(restaurantId, callBack){
			try{
				var self = this;
				var resultArry = [];
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    var cursor = db.collection(self.creditsTransactionDB).find({"restaurant_id": restaurantId});
				    cursor.forEach(function(doc, err){
				      assert.equal(null, err);
				      resultArry.push(doc);
				    },
				    function(){
				      db.close();
				      if(resultArry.length){
				      	callBack(null, resultArry);
				      }else{
				      	callBack(true, "notransactions");
				      }
				    });

				  });
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		getMarvelsBalance : function(restaurantId, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					db.collection(self.restaurantDB).findOne({"_id": restaurantId}, function(er, result) {
						db.close();
						if (er){
							callBack(true, null);
						}else{
							if(result == null){
								callBack(null, 0);
							}else if(result.hasOwnProperty("marvels_balance")){
								callBack(null, result.marvels_balance);
							}else{
								callBack(null, 0);
							}
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		GetRestaurantData : function(restaurantId, callBack){
			try{
				var self = this;
				var resultArry = [];
				resultArry.push({"month":0, "day":0});
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					db.collection(self.claimTransactionsDB).find({"restaurant_id": restaurantId, "transaction_month": new Date().getMonth()+1}).count(function(er, result) {
						
						resultArry[0].month = result;

					});

					db.collection(self.claimTransactionsDB).find({"restaurant_id": restaurantId, "transaction_month": new Date().getDate()}).count(function(er, result) {
						resultArry[0].day = result;
						console.log(resultArry);
						callBack(null, resultArry);
					});
					
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		addCreditToCampaign: function(restaurantId, amount, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					db.collection(self.restaurantDB).updateOne({"_id": restaurantId}, { $inc: {Campaign_Credits: parseInt(amount), Wallet: -1*parseInt(amount) } }, function(err, result){
						db.close();
						console.log(err);
						if (err){
							callBack(true, null);
						}else{
							callBack(null, "success");
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		getCreditsBalance : function(restaurantId, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					db.collection(self.restaurantDB).findOne({"_id": restaurantId}, function(er, result) {
						db.close();
						if (er){
							callBack(true, null);
						}else{
							if(result == null){
								callBack(null, 0);
							}else if(result.hasOwnProperty("Campaign_Credits")){
								callBack(null, result.Campaign_Credits);
							}else{
								callBack(null, 0);
							}
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		updateMarvelsBalance : function(details, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					var newvalues = { $set: {marvels_balance: details.marvels_balance } };
					db.collection(self.restaurantDB).updateOne({"_id": details.restaurantId}, newvalues, function(err, result){
						db.close();
						if (err){
							callBack(true, null);
						}else{
							callBack(null, true);
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		updateCreditsBalance : function(details, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					var newvalues = { $set: {Campaign_Credits: details.Campaign_Credits } };
					if(details.hasOwnProperty("adSharing")){
						newvalues = { $set: {Campaign_Credits: details.Campaign_Credits, adSharing : false } };
					}
					db.collection(self.restaurantDB).updateOne({"_id": details.restaurantId}, newvalues, function(err, result){
						db.close();
						if (err){
							callBack(true, null);
						}else{
							callBack(null, true);
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		updateRestaurantDeals : function(details, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					var newvalues = { $set: {deals: details.deal } };
					db.collection(self.restaurantDB).updateOne({"_id": details.restaurantId}, newvalues, function(err, result){
						db.close();
						if (err){
							callBack(true, null);
						}else{
							callBack(null, true);
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		getRestaurantDeals : function(restaurantId, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					db.collection(self.restaurantDB).findOne({"_id": restaurantId}, function(er, result) {
						db.close();
						if (er){
							callBack(true, null);
						}else{
							if(result == null){
								callBack(null, 0);
							}else if(result.hasOwnProperty("deals")){
								callBack(null, result.deals);
							}else{
								callBack(null, []);
							}
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		getOlezBalance : function(OlezObj, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					db.collection(self.olezDB).findOne({"restaurant_id": OlezObj.restaurant_id, "user_id" : OlezObj.user_id}, function(er, result) {
						db.close();
						if (er){
							callBack(true, null);
						}else{
							if(result == null){
								callBack(null, "resultNull");
							}else if(result.hasOwnProperty("olez_balance")){
								callBack(null, result.olez_balance);
							}else{
								callBack(null, 0);
							}
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		olezInsert : function(OlezObj, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    db.collection(self.olezDB).insertOne(OlezObj, function(err, result){
				    	db.close();
				    	if(err){
				   			callBack(true, null);
				    	}else{
				    		callBack(null, true);
				    	}
				    });
				});
			}catch(e){
				console.log("error : "+e);
				callBack(true, null);
			}
		},

		redeem : function(details, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    db.collection(self.redemptionDB).insertOne(details, function(err, result){
				    	db.close();
				    	if(err){
				   			callBack(true, null);
				    	}else{
				    		callBack(null, true);
				    	}
				    });
				});
			}catch(e){
				console.log("error : "+e);
				callBack(true, null);
			}
		},

		redemptionHistory : function(details, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db){
			    	assert.equal(null, err);
			    	db.collection(self.redemptionDB).findOne({"restaurant_id": details.restaurant_id, "deal_id": details.deal_id, "userid" : details.userid}, function(er, result) {
						db.close();
						if (er){
							callBack(true, null);
						}else{
							if(result == null){
								callBack(null, 0);
							}else if(result.hasOwnProperty("deal_id")){
								callBack(null, details.deal_id);
							}
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		updateMarvolezBalance : function(details, callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					var newvalues = { $set: {marvolez_credits: details.marvolez_credits, last_updated: details.last_updated } };
					db.collection(self.marvolezCreditsDB).updateOne({"_id": details._id}, newvalues, function(err, result){
						db.close();
						if (err){
							callBack(true, null);
						}else{
							callBack(null, true);
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		getMarvolezBalance : function(callBack){
			try{
				var self = this;
				mongo.connect(url, function(err, db) {
					assert.equal(null, err);
					db.collection(self.marvolezCreditsDB).findOne({}, function(er, result) {
						db.close();
						if (er){
							callBack(true, null);
						}else{
							if(result == null){
								callBack(null, "resultNull");
							}else{
								callBack(null, result);
							}
						}
					});
				});
			}catch(e){
				console.log("error occured : "+e);
				callBack(true, null);
			}
		},

		insertFirstMarvolezBalance :function(details, callBack){
			try{
				var self = this;
				var dbName = self.marvolezCreditsDB;
				mongo.connect(url, function(err, db){
				    assert.equal(null, err);
				    db.collection(dbName).insertOne(details, function(err, result){
				    	db.close();
				    	if(err){
				   			callBack(true, null);
				    	}else{
				    		callBack(null, result.ops[0]._id);
				    	}
				    });
				});
			}catch(e){
				console.log("error : "+e);
				callBack(true, null);
			}
		}


	}
	return marvolezApp;
}