var async = require('async');
var jwt = require('jsonwebtoken');
var config = require('../config');
var hash = require('hash.js');

module.exports = {
	getHash: function (stringVar) {
		return hash.sha256().update(stringVar).digest('hex');
	},

	getMenu: function (userid, mongo, url, ObjectID, resultArry, cb) {
		var finalArray = [];
		var counter = 1;
		async.forEachSeries(resultArry, function (item, callback) {
			var finalItem = item;
			if (item.hasOwnProperty("menuid")) {
				console.log(item.menuid);
				mongo.connect(url, function (err, db) {
					var myquery = { "_id": new ObjectID(item.menuid) };
					var cursor = db.collection('menus').find(myquery);
					cursor.forEach(function (doc, err) {
						var menu = doc.Menu;
						var menuItems = [];
						console.log('iterating through menus for ' + doc.Restaurant);
						for (var i = 0; i < menu.length; i++) {
							if (menu[i].hasOwnProperty("Price") && !(menu[i].Price.constructor === Array)) {
								var priceArr = [];
								var priceItem = menu[i].Price;
								priceArr.push(priceItem);
								menu[i].Price = priceArr;
							}
							if (menu[i].hasOwnProperty("Scroll Items") && menu[i]["Scroll Items"] != "FALSE") {
								menuItems.push(menu[i]);
							}
						}
						menu = menuItems;
						finalItem.Menu = menu;
					},
						function () {
							if (userid != null) {
								db.collection("olez").findOne({ "restaurant_id": new ObjectID(finalItem._id), "user_id": userid }, function (er, result) {
									db.close();
									if (er) throw er;
									if (result != null && result.hasOwnProperty("olez_balance")) {
										console.log("olez bal : " + result.olez_balance);
										finalItem.olez_balance = result.olez_balance;
									}
									finalArray.push(finalItem);
									callback();
								});
							} else {
								db.close();
								finalArray.push(finalItem);
								callback();
							}
						});
				});
			} else {
				finalArray.push(finalItem);
				callback();
			}
		}, function () {
			cb(null, finalArray);
		});
	},

	configure: function (app, mongo, url, assert, ObjectID, geodist) {
		var self = this;
		var restaurantApp = require('../modules/restaurantmodule')(mongo, url, assert);

		app.post('/getMenuById', function (req, res) {
			try {
				if (req.body.hasOwnProperty("menuid")) {

					var menuid = req.body.menuid;
					if (menuid != null && menuid != "") {
						menuid = new ObjectID(menuid);
						restaurantApp.getMenu(menuid, function (err, result) {
							if (err != null) {
								if (result == "nomenu") {
									console.log("No record for given menu id");
									res.send("false");
								} else {
									console.log("Error : " + err);
									res.send("false");
								}
							} else {
								console.log("menuid details found");
								for (var i = 0; i < result.length; i++) {
									if (result[i].hasOwnProperty("Menu")) {
										for (var j = 0; j < result[i].Menu.length; j++) {
											if (result[i].Menu[j].hasOwnProperty("Price") && !(result[i].Menu[j].Price.constructor === Array)) {
												var priceArr = [];
												var priceItem = result[i].Menu[j].Price;
												priceArr.push(priceItem);
												result[i].Menu[j].Price = priceArr;
											}
										}
									}
								}
								res.json(result);
							}
						});
					} else {
						console.log("menuid empty");
						res.send("false");
					}
				} else {
					console.log("menuid not sent");
					res.send("false");
				}
			} catch (er) {
				console.log("error occured : " + er);
				res.send("false");
			}
		});

		//API TO GET Particular INFORMATION FROM THE RESTAURANT TABLE for web
		app.post('/getByLocation', function (req, res) {
				var userid = null;
				
				var resultArry = [];
				mongo.connect(url, function (err, db) {
					assert.equal(null, err);
					var cursor = db.collection('Restaurants').find(
						{ "Location": { $near: [parseFloat(req.body.long), parseFloat(req.body.lat)], $maxDistance: (parseInt(req.body.distance) / 63710) } }
					).limit(parseInt(req.body.numOfRestaurants));
					cursor.forEach(function (doc, err) {
						assert.equal(null, err);
						const retData = Object.assign({}, doc, {
							Distance: geodist({ lat: req.body.lat, lon: req.body.long }, { lat: doc.Location[1], lon: doc.Location[0] }, { unit: 'meters' })
						});
						resultArry.push(retData);
					},
						function () {
							db.close();
							// self.getMenu(userid, mongo, url, ObjectID, resultArry, function (error, response) {
							// 	res.json(response);
							// });
							res.json(resultArry);
						});
				});
		});

		//API for adding owner to the restaurant after verification
		app.post('/addOwner', function (req, res) {
			if (req.headers.hasOwnProperty('token')) {
				var token = req.headers.token;
				jwt.verify(token, config.jwt.secret, function (err, decoded) {
					if (err) {
						return res.json({ status: false, message: 'Failed to authenticate token.' });
					} else {
						// if everything is good, save to request for use in other routes
						req.decoded = decoded;
						console.log(req.decoded.email);
						//return res.json(decoded);
						var ownerHash = self.getHash(req.decoded.email);
						console.log(ownerHash);
						if (req.body.hasOwnProperty('hexCode')) {
							if (req.body.hexCode == "TiKfGRskuIMB2h7iFOyszpCE3Sxpgp") {
								if (req.body.hasOwnProperty("restaurant_id") && req.body.restaurant_id != null) {
									var detailsObject = {
										_id: new ObjectID(req.body.restaurant_id),
										ownerHash: ownerHash
									};
									restaurantApp.addOwner(detailsObject, function (err1, result1) {
										if (err1 != null) {
											console.log('err');
											return res.json({ status: false, err: 'restaurant_id not sent' });
										} else {
											return res.json({ status: true, message: 'owner added successfully', hash: ownerHash });
										}
									});
								} else {
									console.log('restaurant_id not sent');
									return res.json({ status: false, err: 'restaurant_id not sent' });
								}
							} else {
								console.log('hexCode invalid');
								return res.json({ status: false, err: 'invalid hexCode' });
							}
						} else {
							console.log('hexCode not sent');
							return res.json({ status: false, err: 'hexCode not sent' });
						}
					}
				});
			} else {
				console.log('token not sent');
				return res.json({ status: false, err: "Token not sent !!!" });
			}
		});

		app.post('/getClaimedRestaurants', function (req, res) {
			if (req.headers.hasOwnProperty('token')) {
				var token = req.headers.token;
				jwt.verify(token, config.jwt.secret, function (err, decoded) {
					if (err) {
						return res.json({ status: false, message: 'Failed to authenticate token.' });
					} else {
						// if everything is good, save to request for use in other routes
						req.decoded = decoded;
						console.log(req.decoded.email);
						//return res.json(decoded);
						var ownerHash = self.getHash(req.decoded.email);
						console.log(ownerHash);
						restaurantApp.claimedRestaurant(ownerHash, function (err1, result1) {
							if (err1 != null) {
								console.log('no restaurants');
								return res.json({ status: false, err: 'invalid hexCode' });
							} else {
								return res.json({ status: true, hash: ownerHash, result: result1 });
							}
						});
					}
				});
			} else {
				console.log('token not sent');
				return res.json({ status: false, err: "Token not sent !!!" });
			}
		});


		app.post('/getSingleClaimedRestaurants', function (req, res) {
			if (req.headers.hasOwnProperty('token')) {
				var token = req.headers.token;
				jwt.verify(token, config.jwt.secret, function (err, decoded) {
					if (err) {
						return res.json({ status: false, message: 'Failed to authenticate token.' });
					} else {
						// if everything is good, save to request for use in other routes
						req.decoded = decoded;
						console.log(req.decoded.email);
						//return res.json(decoded);
						var ownerHash = self.getHash(req.decoded.email);
						console.log(ownerHash);
						restaurantApp.claimedSingleRestaurant(ownerHash, new ObjectID(req.body.restaurant_id), function (err1, result1) {
							if (err1 != null) {
								console.log('no restaurants');
								return res.json({ status: false, err: 'invalid hexCode' });
							} else {
								return res.json({ status: true, hash: ownerHash, result: result1 });
							}
						});
					}
				});
			} else {
				console.log('token not sent');
				return res.json({ status: false, err: "Token not sent !!!" });
			}
		});

		app.post('/addedRestaurant', function (req, res) {
			if (req.headers.hasOwnProperty('token')) {
				var token = req.headers.token;
				jwt.verify(token, config.jwt.secret, function (err, decoded) {
					if (err) {
						return res.json({ status: false, message: 'Failed to authenticate token.' });
					} else {
						// if everything is good, save to request for use in other routes
						req.decoded = decoded;
						var verified = false;
						if (req.decoded.type == "admin")
							verified = true;
						//return res.json(decoded);
						var newaddedRestaurant = {
							Name: req.body.name,
							Address: req.body.address,
							ContactPerson: req.body.contactPerson,
							City: req.body.city,
							State: req.body.state,
							country: req.body.country,
							Phone: req.body.phone,
							Pincode: parseInt(req.body.pincode),
							Email: req.body.email,
							Location: [parseFloat(req.body.long), parseFloat(req.body.lat)],
							twitterPage: req.body.twitterPage,
							instagramPage: req.body.instagramPage,
							facebookPage: req.body.facebookPage,
							bearing: "",
							position: "",
							BusinessHours: req.body.businesshour,
							MapURL: req.body.mapurl,
							Specials: req.body.specials,
							Website: req.body.website,
							Wallet: 0,
							Campaign_Credits: 0

						};
						restaurantApp.addUnverified(newaddedRestaurant, function (err1, result1) {
							if (err1 != null) {
								console.log('restaurant addition failed');
								return res.json({ status: false, err: "restaurant addition failed !!!" });
							} else {
								return res.json({ status: true, message: "restaurant added !!!" });
							}
						}, verified);
					}
				});
			} else {
				console.log('token not sent');
				return res.json({ status: false, err: "Token not sent !!!" });
			}
		});

		app.post('/verifyRestaurant', function (req, res) {
			// if everything is good, save to request for use in other routes
			restaurantApp.verifyRest(new ObjectID(req.body.restaurant_id), function (err1, result1) {
				if (err1 != null) {
					console.log('restaurant addition failed');
					return res.json({ status: false, err: "restaurant addition failed !!!" });
				} else {
					return res.json({ status: true, message: "restaurant added !!!" });
				}
			});


		});

		// app.post('/verifyRestaurant', function(req, res){
		// 	if(req.headers.hasOwnProperty('token')){
		// 		var token = req.headers.token;
		// 		jwt.verify(token, config.jwt.secret, function(err, decoded) {    
		// 			if (err) {
		// 				return res.json({status: false, message: 'Failed to authenticate token.'});
		// 			} else {
		// 				// if everything is good, save to request for use in other routes
		// 				req.decoded = decoded;
		// 				var verified = false;
		// 				if(req.decoded.type == "admin")
		// 					verified = true;
		// 				if(verified){
		// 					if(req.body.hasOwnProperty('restaurant_id') && req.body.restaurant_id != null && req.body.restaurant_id != ""){
		// 						var restaurantObject = {
		// 							_id : new ObjectID(req.body.restaurant_id)
		// 						};
		// 						restaurantApp.getUnverifiedRestaurant(restaurantObject._id, function(err1, result1){
		// 							if(err1 != null){
		// 								if(err1 == "norestaurant"){
		// 									return res.json({ status: false, message: 'No such Restaurant in DB'});
		// 								}else{
		// 									return res.json({ status: false, message: 'Some internal error in DB'});
		// 								}
		// 							}else{
		// 								delete result1[0]._id;
		// 								restaurantObject = result1[0];
		// 								restaurantApp.addUnverified(restaurantObject, function(err2, result2){
		// 						        	if(err2 != null){
		// 						        		console.log('restaurant verification failed');
		// 										return res.json({status: false, err: "restaurant verification failed !!!"});
		// 						        	}else{
		// 						        		return res.json({status: true, message: "restaurant verified !!!"});
		// 						        	}
		// 						        }, verified);
		// 							}
		// 						});
		// 					}else{
		// 						return res.json({ status: false, message: 'Please provide with restaurant id'});
		// 					}
		// 				}else{
		// 					return res.json({ status: false, message: 'Please use an admin account.'});
		// 				}
		// 			}
		// 		});
		// 	}else{
		// 		return res.json({ status: false, message: 'Failed to authenticate token.'});
		// 	}
		// });

		app.post('/toggleAdSharing', function (req, res) {
			try {
				if (req.body.hasOwnProperty('restaurant_id') && req.body.restaurant_id != null && req.body.restaurant_id != "") {
					var restaurantObject = {
						_id: new ObjectID(req.body.restaurant_id)
					};
					restaurantApp.getAdSharing(restaurantObject._id, function (err1, result1) {
						if (err1 != null) {

						} else {
							restaurantObject.adSharing = !result1;
							restaurantApp.updateAdSharing(restaurantObject, function (err2, result2) {
								if (err2 != null) {

								} else {
									if (result1)
										return res.json({ status: true, message: 'adSharing deactivated' });
									else
										return res.json({ status: true, message: 'adSharing activated' });
								}
							});
						}
					});
				} else {
					return res.json({ status: false, message: 'Please provide with restaurant id' });
				}
			} catch (e) {
				return res.json({ status: false, message: 'some error' });
			}
		});

	}
}