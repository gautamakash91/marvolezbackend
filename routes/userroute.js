var hash = require('hash.js');
var jwt = require('jsonwebtoken');
var config = require('../config');
var nodemailer = require('nodemailer');
const conf = require('./creds');
var dat = require('date-and-time');

module.exports = {
	getHash : function(stringVar){
		return hash.sha256().update(stringVar).digest('hex');
	},

	jwtSign: function(obj){
		var token = jwt.sign(obj, config.jwt.secret);
		return token;
	},

	sendEmail : function(to, subject, body){
		var transporter = nodemailer.createTransport({
			service: 'Gmail',
			auth: {
				user: conf.user,
				pass: conf.pass
			}
		});

		console.log('emailer created');
		transporter.sendMail({
			from: 'Marvolez Password Reset',
			to: to,
			subject: subject,
		//   text: body,
		html: body,
	});
	},

	configure: function (app, mongo, ObjectID, url, assert) {
		var self = this;
		var userApp = require('../modules/usermodule')(mongo, url, assert);

		app.post('/addUserDetails', function(req, res){
			try{
				if(req.body.hasOwnProperty("firstname") && req.body.hasOwnProperty("lastname") && req.body.hasOwnProperty("email") && req.body.hasOwnProperty("phone") && req.body.hasOwnProperty("status")){
					var newUserDetails = {
						email: req.body.email,
						firstname: req.body.firstname,
						lastname: req.body.lastname,
						phone: req.body.phone,
						status: req.body.status,
						registration_date: ""+ (new Date())
					};
					if(req.body.hasOwnProperty("social")){
						newUserDetails.social = req.body.social;
						newUserDetails.password = self.getHash(req.body.email);
					}else{
						if(req.body.hasOwnProperty("password") && !req.body.hasOwnProperty("social")){
							newUserDetails.password = self.getHash(req.body.password);
						}else{
							return res.json({status:false,Message: "password not provided"});
						}
					}
					userApp.isUserExist(newUserDetails, function(e, r){
						if(e != null){
							return res.json({status:false,Message: "error connecting database"});
						}else{
							if(r == 0){
								userApp.addUser(newUserDetails, function(err, result){
									if(err != null){
										console.log("Error : "+err);
										return res.json({status:false,Message: err});
									}else{
										console.log("user registered successfully");
										return res.json({status:true,Message: "user registered successfully"});
									}
								});
							}else{
								return res.json({status:false,Message: r.email + " already exists"});
							}
						}
					});
				}else{
					console.log("some empty field");
					return res.json({status:false,Message: "some empty field"});
				}
			}catch(er){
				console.log("error occured : "+er);
				return res.json({status:false,Message: er});
			}
		});



// API FOR USEROPEND

app.post('/useropend', function(req, res){
	try{
		if (req.body.hasOwnProperty("user_id")) {

			var dat = require('date-and-time');
			var date = dat.format(new Date(), 'DD-MM-YYYY');
// console.log(formatted);

var newUserDetails = {
	user_id: req.body.user_id,
	current_date: date,
	location: [parseFloat(req.body.long), parseFloat(req.body.lat)],
	pincode: req.body.pincode
};

userApp.Useropend(newUserDetails, function(err, result){
	if(err){
		return res.json({status:false,Message: err});
	}else{
		var response = {
			status: true,
			id: result.insertedId
		}
		return res.json(response);
	}
});
}else{
	console.log("some empty field");
	return res.json({status:false,Message: "some empty field"});
}
}catch(er){
	console.log("error occured : "+er);
	return res.json({status:false,Message: er});
}
});

// API FOR UPDATEUSER

app.post('/updateuser', function(req, res){
	try{
		if (req.body.hasOwnProperty("id")&&req.body.hasOwnProperty("email")&&req.body.hasOwnProperty("name")&&req.body.hasOwnProperty("phone")&&req.body.hasOwnProperty("login_date")) {


			userApp.Userupdate(newUserDetails, function(err, result){
				if(err){
					return res.json({status:false,Message: err});
				}else{
					var response = {
						status: true,

					}
					return res.json(response);
				}
			});
}else{
	console.log("some empty field");
	return res.json({status:false,Message: "some empty field"});
}
}catch(er){
	console.log("error occured : "+er);
	return res.json({status:false,Message: er});
}
});

// API FOR USERINSTALLED

app.post('/userinstalled', function(req, res){
	try{
		if (req.body.hasOwnProperty("device_name") && req.body.hasOwnProperty("device_token")) {
			var dat = require('date-and-time');
			var date = dat.format(new Date(), 'DD-MM-YYYY');
			var newUserDetails = {
				device_name: req.body.device_name,
				device_token: req.body.device_token,
				installed_date:date
			};


			userApp.Userinstalled(newUserDetails, function(err, result){
				if(err){
					return res.json({status:false,Message: err});
				}else{
					var response = {
						status: true,
						id: result.insertedId
					}
					return res.json(response);
				}
			});



		}else{
			console.log("some empty field");
			return res.json({status:false,Message: "some empty field"});
		}
	}catch(er){
		console.log("error occured : "+er);
		return res.json({status:false,Message: er});
	}
});





app.post('/login', function(req, res){
	try{
				/*
				jwt.verify(authToken, config.jwt.secret, function(err, decoded) {      
			      if (err) {
			        return res.json({ success: false, message: 'Failed to authenticate token.' });    
			      } else {
			        // if everything is good, save to request for use in other routes
			        req.decoded = decoded;
			        console.log(req.decoded);   
			        return res.json(decoded);
			        next();
			      }
			    });
			    */
			    if(req.body.hasOwnProperty("social") && req.body.social == 1){
			    	req.body.password = req.body.email;
			    }
			    if(req.body.hasOwnProperty("email") && req.body.hasOwnProperty("password")){
			    	if(req.body.email != null && req.body.email != "" && req.body.password != null &&req.body.password != ""){
			    		var userDetails = {
			    			email : req.body.email,
			    			password : self.getHash(req.body.password)
			    		};
			    		console.log(userDetails)
			    		userApp.login(userDetails, function(err, result){
			    			if(err != null){
			    				console.log("error while retrieving from db");
			    				return res.json({status:false,Message: "error while retrieving from db"});
			    			}else{
								// console.log(result[0].valid);
								if (result[0].valid == 'true'){
									//   console.log(result.valid);
									var token = self.jwtSign(result[0].token);
									result[0].token = token;
									return res.json({status:true,result: result[0]});
								}else{
									return res.json({status:false,Message: "incorrect email or password"});
								}
							}
						});
							// 	console.log(result.valid);

							// 	  if (result.valid == true){
							// 	  var token = self.jwtSign(result[0].token);
							// 	  result[0].token = token;
							// 	  return res.json({status:true,result: result[0]});
							// 	//res.json(result);	

							// 	   }else{
							// 		   console.log(result.valid == false);
							// 		return res.json({status:false,Message: "email or password is not match"});
							// 	}

							// }

						//});
						
					}else{
						console.log("email or password is null");
						return res.json({status:false,Message: "email or password is null"});
					}
				}else{
					console.log("email or password is missing");
					return res.json({status:false,Message: "email or password is missing"});
				}
			}catch(er){
				console.log("err occured : " + er);
				return res.json({status:false,Message: er});
			}
		});

app.post('/isSocialUser', function(req, res){
	try{
		if(req.body.hasOwnProperty("email")){
			if(req.body.email != null && req.body.email){
				var userDetails = {
					email : req.body.email
				};
				userApp.isUserExist(userDetails, function(e, r){
					if(e != null){
						return res.json({status:false,Message: "error connecting database"});
					}else{
						if(r == 0){
							return res.json({status:false,Message: "user is not registered"});
						}else{
							if(r.hasOwnProperty("social") && r.social != "" && r.social != null){
								return res.json({status:true, isSocial:true, Message: "user is registered as "+r.social});
							}else{
								return res.json({status:true, isSocial:false, Message: "user is registered as regular user"});
							}
						}
					}
				});
			}else{
				console.log("email is null");
				return res.json({status:false,Message: "email is null"});
			}
		}else{
			console.log("emailis missing");
			return res.json({status:false,Message: "email is missing"});
		}
	}catch(er){
		console.log("err occured : " + er);
		return res.json({status:false,Message: er});
	}
});

app.post('/forgotPassword', function(req, res){
	try{
		if(req.body.hasOwnProperty("email")){
			if(req.body.email != null && req.body.email){
				var userDetails = {
					email : req.body.email
				};
				console.log(userDetails);
				userApp.isUserExist(userDetails, function(err1, result1){
					if(err1 != null){
						console.log(result1);
						return res.json({status:false,Message: "some error"});
					}else{
						var d = new Date();

						var tokenObject = {
							email : result1.email,
							transaction_time: d.toString(),
							type : 'user'
						};

						var token = self.jwtSign(tokenObject);

						var body = 'Hi, <br>You have requested to reset your password. <br>To reset your password click <a href="https://www.marvolez.com/forgot_password.html?hash='+token+'">HERE</a><br>If you have not initiated the reset password process then please delete this email';
						self.sendEmail(tokenObject.email, "Reset Password", body);
						return res.json({status : true, email : body});
					}
				});
			}else{
				console.log("email is null");
				return res.json({status:false,Message: "email or password is null"});
			}
		}else{
			console.log("email is missing");
			return res.json({status:false,Message: "email or password is missing"});
		}
	}catch(er){
		console.log("err occured : " + er);
		return res.json({status:false,Message: er});
	}
});

		// app.post('/forgotPassword', function(req, res){
		// 	try{
		// 		if(req.body.hasOwnProperty("email")){
		// 			if(req.body.email != null && req.body.email){
		// 				var userDetails = {
		// 					email : req.body.email
		// 				};
		// 				console.log(userDetails);
		// 				clientApp.ifClientExists(userDetails.email, function(err1, result1){
		// 					if(err1 != null){
		// 						console.log(result1);
		// 						return res.json({status:false,Message: "some error"});
		// 					}else{
		// 						var d = new Date();

		// 						var tokenObject = {
		// 							email : result1.email,
		// 							type : 'client',
		// 							transaction_time: d.toString()
		// 						};

		// 						var token = self.jwtSign(tokenObject);

		// 						var body = 'Hi, <br>You have requested to reset your password. <br>To reset your password click <a href="https://www.marvolez.com/forgot_password.html?hash='+token+'">HERE</a><br>If you have not initiated the reset password process then please delete this email';
		// 						self.sendEmail(tokenObject.email, "Reset Password", body);
		// 						return res.json({status : true, email : body});
		// 					}
		// 				});
		// 			}else{
		// 				console.log("email is null");
		// 				return res.json({status:false,Message: "email or password is null"});
		// 			}
		// 		}else{
		// 			console.log("email is missing");
		// 			return res.json({status:false,Message: "email or password is missing"});
		// 		}
		// 	}catch(er){
		// 		console.log("err occured : " + er);
		// 		return res.json({status:false,Message: er});
		// 	}
		// });

		/*
		app.post('/verifyPasswordHash', function(req, res){
			try{
				if(req.headers.hasOwnProperty('hash')){
					var token = req.headers.hash;
					jwt.verify(token, config.jwt.secret, function(err, decoded) {
						if (err) {
					        return res.json({ success: false, message: 'Failed to authenticate hash.' });    
					    } else {
					        // if everything is good, save to request for use in other routes
					        req.decoded = decoded;
					        console.log(req.decoded);   
					        //return res.json(decoded)
					        var userid = null;
							if(req.decoded.hasOwnProperty("email")){
								var email = req.decoded.email;
								userApp.ifUserExists({email : email}, function(err1, result1){
									if(err1 != null){
										console.log(result1);
										return res.json({status:false,Message: "verify fail"});
									}else{
										return res.json({status : true, Message : "verified"});
									}
								});
							}
						}
					});
				}else{
					return res.json({ success: false, message: 'Failed to authenticate hash.' }); 
				}
			}catch(er){
				console.log("err occured : " + er);
				return res.json({status:false,Message: er});
			}
		});
		
		
		app.post('/resetPassword ', function(req, res){
			try{
				if(req.body.hasOwnProperty("email") && req.body.hasOwnProperty("password")){
					if(req.body.email != null && req.body.email != "" && req.body.password != null &&req.body.password != ""){
						var userDetails = {
							email : req.body.email,
							password : self.getHash(req.body.password)
						};
						console.log(userDetails)
						userApp.updatePassword(userDetails, function(err, result){
							if(err != null){
								console.log("error while retrieving from db");
								return res.json({status:false,Message: "error"});
							}else{
								console.log("success retrieval from db");
								
								return res.json({status:true,Message: "password changed"});
								//res.json(result);
							}
						});
					}else{
						console.log("email or password is null");
						return res.json({status:false,Message: "email or password is null"});
					}
				}else{
					console.log("email or password is missing");
					return res.json({status:false,Message: "email or password is missing"});
				}
			}catch(er){
				console.log("err occured : " + er);
				return res.json({status:false,Message: er});
			}
		});
		*/

		app.post('/connectFacebook', function(req, res){
			if(req.headers.hasOwnProperty('token')){
				var token = req.headers.token;
				jwt.verify(token, config.jwt.secret, function(err, decoded) {      
					if (err) {
						return res.json({ status: false, message: 'Failed to authenticate token.' });    
					} else {
						req.decoded = decoded;
						console.log(req.decoded);
						var FacebookFriends = req.body.FacebookFriends;
						userApp.connectFacebook(new ObjectID(req.decoded._id),FacebookFriends, function(err1, result1){
							if(err1 != null){
								return res.json({status:false, Message: "some error"});
							}else{
								return res.json({status : true, Message: "Friends Updated"});
							}
						});
					}
				});
			}else{
				console.log('token not sent');
				return res.json({status: false, err: "Token not sent !!!"});
			}
		});

		app.post('/connectTwitter', function(req, res){
			if(req.headers.hasOwnProperty('token')){
				var token = req.headers.token;
				jwt.verify(token, config.jwt.secret, function(err, decoded) {      
					if (err) {
						return res.json({ status: false, message: 'Failed to authenticate token.' });    
					} else {
						req.decoded = decoded;
						console.log(req.decoded);
						var TwitterFriends = req.body.TwitterFriends;
						userApp.connectTwitter(new ObjectID(req.decoded._id),TwitterFriends, function(err1, result1){
							if(err1 != null){
								return res.json({status:false, Message: "some error"});
							}else{
								return res.json({status : true, Message: "Friends Updated"});
							}
						});
					}
				});
			}else{
				console.log('token not sent');
				return res.json({status: false, err: "Token not sent !!!"});
			}
		});
	}
}



