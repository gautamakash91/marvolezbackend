var hash = require('hash.js');
var jwt = require('jsonwebtoken');
var config = require('../config');
var nodemailer = require('nodemailer');
var xoauth2 = require('xoauth2');
const conf = require('./creds');

module.exports = {
	getHash : function(stringVar){
        return hash.sha256().update(stringVar).digest('hex');
    },

    jwtSign: function(obj){
		var token = jwt.sign(obj, config.jwt.secret);
		return token;
	},

	sendEmail : function(to, subject, body){
		var transporter = nodemailer.createTransport({
		    service: 'Gmail',
		    auth: {
		        user: conf.user,
		        pass: conf.pass
		    }
		});

		console.log('emailer created');
		transporter.sendMail({
		from: 'Marvolez Password Reset',
		  to: to,
		  subject: subject,
		//   text: body,
		  html: body,
		});
	},

	configure: function (app, mongo, ObjectID, url, assert) {
		var self = this;
		var clientApp = require('../modules/clientmodule')(mongo, url, assert);
		var userApp = require('../modules/usermodule')(mongo, url, assert);

		app.post('/registerClient', function(req, res){
			try{
				if(req.body.hasOwnProperty("firstname") && req.body.hasOwnProperty("lastname") && req.body.hasOwnProperty("email") && req.body.hasOwnProperty("password") && req.body.hasOwnProperty("status") && req.body.hasOwnProperty("type")){

					var newClientDetails = {
						email: req.body.email,
						firstname: req.body.firstname,
						lastname: req.body.lastname,
						status: req.body.status,
						password: self.getHash(req.body.password),
						type: req.body.type
					};

					clientApp.registerClient(newClientDetails, function(err, result){
						if(err != null){
							console.log("Error : "+err);
							res.json({status: false});
						}else{
							console.log("client registered successfully");
							res.json({status: true});
						}
					});
				}else{
					console.log("some empty field");
					res.send("false");
				}
			}catch(er){
				console.log("error occured : "+er);
				res.send("false");
			}
		});

		app.post('/clientLogin', function(req, res){
			try{
			    if(req.body.hasOwnProperty("social") && req.body.social == 1){
			    	req.body.password = req.body.email;
			    }
				if(req.body.hasOwnProperty("email") && req.body.hasOwnProperty("password")){
					if(req.body.email != null && req.body.email != "" && req.body.password != null &&req.body.password != ""){
						var userDetails = {
							email : req.body.email,
							password : self.getHash(req.body.password)
						};
						console.log(userDetails);
						clientApp.clientLogin(userDetails, function(err, result){
							if(err != null){
								console.log("error while retrieving from db");
								return res.json({status:false,Message: "error while retrieving from db"});
							}else{
								console.log("success retrieval from db");
								
								var token = self.jwtSign(result[0].token);
								result[0].token = token;
								return res.json({status:true,result: result[0]});
								//res.json(result);
							}
						});
					}else{
						console.log("email or password is null");
						return res.json({status:false,Message: "email or password is null"});
					}
				}else{
					console.log("email or password is missing");
					return res.json({status:false,Message: "email or password is missing"});
				}
			}catch(er){
				console.log("err occured : " + er);
				return res.json({status:false,Message: er});
			}
		});

		app.post('/forgotClientPassword', function(req, res){
			try{
				if(req.body.hasOwnProperty("email")){
					if(req.body.email != null && req.body.email){
						var userDetails = {
							email : req.body.email
						};
						console.log(userDetails);
						clientApp.ifClientExists(userDetails.email, function(err1, result1){
							if(err1 != null){
								console.log(result1);
								return res.json({status:false,Message: "some error"});
							}else{
								var d = new Date();

								var tokenObject = {
									email : result1.email,
									type : 'client',
									transaction_time: d.toString()
								};

								var token = self.jwtSign(tokenObject);

								var body = 'Hi, <br>You have requested to reset your password. <br>To reset your password click <a href="https://www.marvolez.com/forgot_password.html?hash='+token+'">HERE</a><br>If you have not initiated the reset password process then please delete this email';
								self.sendEmail(tokenObject.email, "Reset Password", body);
								return res.json({status : true, email : body});
							}
						});
					}else{
						console.log("email is null");
						return res.json({status:false,Message: "email or password is null"});
					}
				}else{
					console.log("email is missing");
					return res.json({status:false,Message: "email or password is missing"});
				}
			}catch(er){
				console.log("err occured : " + er);
				return res.json({status:false,Message: er});
			}
		});

		app.post('/resetPassword', function(req, res){
			try{
				if(req.body.hasOwnProperty('hash')){
					var token = req.body.hash;
					jwt.verify(token, config.jwt.secret, function(err, decoded) {
						if (err) {
					        return res.json({ status: false, message: 'Failed to authenticate hash.' });    
					    } else {
					        // if everything is good, save to request for use in other routes
					        req.decoded = decoded;
					        console.log(req.decoded);   
					        //return res.json(decoded)
					        var userid = null;
							if(req.decoded.hasOwnProperty("email") && req.decoded.hasOwnProperty("type")){
								var email = req.decoded.email;
								var type = req.decoded.type;
								if(type == 'client'){
									clientApp.ifClientExists(email, function(err1, result1){
										if(err1 != null){
											console.log(result1);
											return res.json({status:false,Message: "client verify fail"});
										}else{
											if(req.body.password != null &&req.body.password != ""){
												var userDetails = {
													email : email,
													password : self.getHash(req.body.password)
												};
												clientApp.updatePassword(userDetails, function(err, result){
													if(err != null){
														console.log("error while retrieving from db");
														return res.json({status:false,Message: "error"});
													}else{
														console.log("success retrieval from db");
														return res.json({status:true,Message: "password changed"});
														//res.json(result);
													}
												});
											}else{
												return res.json({status:false,Message: "password not sent"});
											}
										}
									});
								}else if(type == 'user'){
									userApp.isUserExist({email : email}, function(err1, result1){
										if(err1 != null){
											console.log(result1);
											return res.json({status:false,Message: "user verify fail"});
										}else{
											if(req.body.password != null &&req.body.password != ""){
												var userDetails = {
													email : email,
													password : self.getHash(req.body.password)
												};
												userApp.updatePassword(userDetails, function(err, result){
													if(err != null){
														console.log("error while retrieving from db");
														return res.json({status:false,Message: "error"});
													}else{
														console.log("success retrieval from db");
														
														return res.json({status:true,Message: "user password changed"});
														//res.json(result);
													}
												});
											}else{
												return res.json({status:false,Message: "password not sent"});
											}
										}
									});
								}else{
									return res.json({status:false,Message: "verification failed"});
								}
							}
						}
					});
				}else{
					return res.json({ status: false, message: 'Failed to authenticate hash.' }); 
				}
			}catch(er){
				console.log("err occured : " + er);
				return res.json({status:false,Message: er});
			}
		});

		app.post('/addSpecial', function(req, res){
			try{
				if(req.body.hasOwnProperty('hash')){
					var token = req.body.hash;
					jwt.verify(token, config.jwt.secret, function(err, decoded) {
						if (err) {
					        return res.json({ status: false, message: 'Failed to authenticate hash.' });    
					    } else {
					        // if everything is good, save to request for use in other routes
					        req.decoded = decoded;
					        console.log(req.decoded);   
					        clientApp.addSpecial(new ObjectID(req.body.restaurantId), req.body.Special, function(err, result){
								if(err != null){
									return res.json({status:false,Message: "error"});
								}else{
									return res.json({status:true,Message: "Special Added"});
								}
							});
						}
					});
				}else{
					return res.json({ status: false, message: 'Missing Parameters' }); 
				}
			}catch(er){
				console.log("err occured : " + er);
				return res.json({status:false,Message: er});
			}
		});
		
	}
}