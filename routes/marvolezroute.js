var jwt = require('jsonwebtoken');
var config = require('../config');
const conf = require('./creds');
var nodemailer = require('nodemailer');

module.exports = {
	getDeal: function(dealArr, deal_id){
		debugger;
		for(var i = 0; i < dealArr.length; i++){
			if(dealArr[i].deal_id.toString() == deal_id.toString()){
				return dealArr[i];
			}
		}
		return -1;
	},
	updateDeals : function(dealArr, deal){
		var newDealArr = [];
		for(var i = 0; i < dealArr.length; i++){
			if(dealArr[i].deal_id.toString() != deal.deal_id.toString()){
				newDealArr.push(dealArr[i]);
			}
		}
		newDealArr.push(deal);
		return newDealArr;
	},
	removeDeal : function(dealArr, deal_id){
		var newDealArr = [];
		for(var i = 0; i < dealArr.length; i++){
			if(dealArr[i].deal_id.toString() != deal_id.toString()){
				newDealArr.push(dealArr[i]);
			}
		}
		return newDealArr;
	},
	jwtSign: function(obj){
		var token = jwt.sign(obj, config.jwt.secret);
		return token;
	},
	sendEmail : function(to, subject, body){
		var transporter = nodemailer.createTransport({
		    service: 'Gmail',
		    auth: {
		        user: conf.user,
		        pass: conf.pass
		    }
		});

		console.log('emailer created');
		transporter.sendMail({
		from: 'Marvolez Password Reset',
		  to: to,
		  subject: subject,
		//   text: body,
		  html: body,
		});
	},
	configure: function (app, mongo, url, assert, ObjectID) {
		var self = this;
		var marvolezApp = require('../modules/marvolezmodule')(mongo, url, assert);
		var userApp = require('../modules/usermodule')(mongo, url, assert);

		app.post('/insertMarvolezCredits', function(req, res){
			var d = new Date();
			var details = {
				marvolez_credits: 1000,
				last_updated: d.toString()
			};

			marvolezApp.insertFirstMarvolezBalance(details, function(err, result){
				if(err != null){

				}else{
					console.log("transaction log successfull at "+d.toString());
					console.log(result);
				}
			});
		});

		app.post('/contactus', function(req, res){
			var msg = "Name: "+req.body.name +"<br>Email: "+req.body.email+"<br>Subject: "+req.body.subject+"<br>Message: "+req.body.message;
			self.sendEmail("implementations@stellosphere.com", "From the contact form in website", msg);
			res.json({status: true});
		});

		app.post('/purchaseMarvels', function(req, res){
			try{
				if(req.body.hasOwnProperty("restaurant_id") && req.body.hasOwnProperty("amount")){
					var d = new Date();

					var newTransactionDetails = {
						restaurant_id: new ObjectID(req.body.restaurant_id),
						amount: req.body.amount,
						credited: true,
						transaction_time: d.toString()
					};
					marvolezApp.transaction(newTransactionDetails, function(err, result){
						if(err != null){
							console.log("Error : "+err);
							return res.json({status:false,Error: err});
						}else{
							console.log("transaction log successfull at "+d.toString());
							marvolezApp.getMarvelsBalance(newTransactionDetails.restaurant_id, function(e, re){
								if(e != null){
									console.log("Error : "+e);
									return res.json({status:false,Error: e});
								}else{
									var totalMarvels = parseInt(newTransactionDetails.amount) + parseInt(re);
									marvolezApp.updateMarvelsBalance({restaurantId : newTransactionDetails.restaurant_id, marvels_balance: totalMarvels}, function(era, resu){
										if(era != null){
											console.log("Error : "+era);
											return res.json({status:false,Error: era});
										}else{
											console.log("marvels purchase successful at "+d.toString());
											return res.json({status:true,Message: "successfully purchased marvels"});
										}
									});
								}
							});
						}
					});
				}else{
					console.log("some empty field");
					return res.json({status:false,Error: "some empty field"});
				}
			}catch(er){
				console.log("error occured : "+er);
				return res.json({status:false,Error: er});
			}
		});

		app.post('/checkBalance', function(req, res){
			try{
				if(req.body.hasOwnProperty("restaurant_id")){
					var d = new Date();

					var details = {
						restaurant_id: new ObjectID(req.body.restaurant_id),
						transaction_time: d.toString()
					};
					marvolezApp.getCreditsBalance(details.restaurant_id, function(err1, result1){
						if(err1 != null){

						}else{
							return res.json({status:true,balance: result1});
						}
					});
				}else{
					console.log("some empty field");
					return res.json({status:false,Error: "some empty field"});
				}
			}catch(er){
				console.log("error occured : "+er);
				return res.json({status:false,Error: er});
			}
		});


		app.post('/startCampaign', function(req, res){
			try{
				if(req.body.hasOwnProperty("restaurant_id")&&req.body.hasOwnProperty("amount")){
					marvolezApp.addCreditToCampaign(new ObjectID(req.body.restaurant_id), req.body.amount, function(err1, result1){
						if(err1 != null){
							console.log("failed");
							res.json({status:false});
						}else{
							// return res.json({status:true,balance: result1});
							console.log("success");
							res.json({status:true});
						}
					});
				}else{
					console.log("some empty field");
					return res.json({status:false,Error: "some empty field"});
				}
			}catch(er){
				console.log("error occured : "+er);
				return res.json({status:false,Error: er});
			}
		});

		app.post('/purchaseAddCredits', function(req, res){
			try{
				if(req.body.hasOwnProperty("restaurant_id") && req.body.hasOwnProperty("amount")){
					var d = new Date();

					var newTransactionDetails = {
						restaurant_id: new ObjectID(req.body.restaurant_id),
						amount: req.body.amount,
						credited: true,
						transaction_time: d.toString()
					};
					marvolezApp.creditTransaction(newTransactionDetails, function(err, result){
						if(err != null){
							console.log("Error : "+err);
							return res.json({status:false,Error: err});
						}else{
							console.log("transaction log successfull at "+d.toString());
							marvolezApp.getCreditsBalance(newTransactionDetails.restaurant_id, function(e, re){
								if(e != null){
									console.log("Error : "+e);
									return res.json({status:false,Error: e});
								}else{
									var totalCredits = parseInt(newTransactionDetails.amount) + parseInt(re);
									marvolezApp.updateCreditsBalance({restaurantId : newTransactionDetails.restaurant_id, add_credits: totalCredits}, function(era, resu){
										if(era != null){
											console.log("Error : "+era);
											return res.json({status:false,Error: era});
										}else{
											console.log("credits purchase successful at "+d.toString());
											return res.json({status:true,Message: "successfully purchased credits"});
										}
									});
								}
							});
						}
					});
				}else{
					console.log("some empty field");
					return res.json({status:false,Error: "some empty field"});
				}
			}catch(er){
				console.log("error occured : "+er);
				return res.json({status:false,Error: er});
			}
		});

		app.post('/getRestaurantsTransactions', function(req, res){
			try{
				if(req.body.hasOwnProperty("restaurant_id")){
					var d = new Date();

					var newTransactionDetails = {
						restaurant_id: new ObjectID(req.body.restaurant_id)
					};
					marvolezApp.getRestaurantsTransactions(newTransactionDetails.restaurant_id, function(e, re){
						if(e != null){
							if(e == "notransactions"){
								console.log("Error : "+e);
								return res.json({status:false,Error: "No transactions available"});
							}else{
								console.log("Error : "+e);
								return res.json({status:false,Error: e});
							}
						}else{
							return res.json({status:true,Message: "successfully retrieved transactions", transactions : re});
						}
					});
				}else{
					console.log("some empty field");
					return res.json({status:false,Error: "some empty field"});
				}
			}catch(er){
				console.log("error occured : "+er);
				return res.json({status:false,Error: er});
			}
		});

		app.post('/getClaimTransactions', function(req, res){
			try{
				if(req.body.hasOwnProperty("restaurant_id")){
					var d = new Date();

					var newTransactionDetails = {
						restaurant_id: new ObjectID(req.body.restaurant_id)
					};
					marvolezApp.getClaimTransactions(newTransactionDetails.restaurant_id, function(e, re){
						if(e != null){
							if(re == "notransactions"){
								console.log("Error : "+e);
								return res.json({status:false,Error: "No transactions available"});
							}else{
								console.log("Error : "+e);
								return res.json({status:false,Error: e});
							}
						}else{
							return res.json({status:true,Message: "successfully retrieved transactions", transactions : re});
						}
					});
				}else{
					console.log("some empty field");
					return res.json({status:false,Error: "some empty field"});
				}
			}catch(er){
				console.log("error occured : "+er);
				return res.json({status:false,Error: er});
			}
		});

		app.post('/createDeal', function(req, res){
			try{
				if(req.body.hasOwnProperty("restaurant_id") && req.body.hasOwnProperty("deal_details") && req.body.hasOwnProperty("quantity") && req.body.hasOwnProperty("start") && req.body.hasOwnProperty("end")){
					var input = req.body;
					if(input.restaurant_id != "" && input.restaurant_id != null && input.deal_details != [] && input.deal_details != null && input.quantity != "" && input.quantity != null && input.start != "" && input.start != null && input.end != "" && input.end != null){
						var d = new Date();

						var details = {
							restaurant_id: new ObjectID(req.body.restaurant_id),
							deal_details: input.deal_details,
							quantity: input.quantity,
							start_time: input.start,
							end_time: input.end,
							transaction_time: d.toString(),
							isActive : true
						};
						marvolezApp.getMarvelsBalance(details.restaurant_id, function(e, r){
							if(e != null){
								console.log("Error : "+e);
								return res.json({status:false,Error: e});
							}else{
								if(parseInt(r) < parseInt(details.quantity)){
									console.log("Error : Insufficient Balance");
									return res.json({status:false,Message: "Insufficient Balance"});
								}else{
									console.log("Marvels Balance is : "+r);
									var totalMarvels = parseInt(r) - parseInt(details.quantity);
									console.log("new balance :" + totalMarvels);
									marvolezApp.transaction(details, function(er, re){

										if(er != null){
											console.log("Error : "+er);
											return res.json({status:false,Error: er});
										}else{
											details.deal_id = new ObjectID(re);
											marvolezApp.updateMarvelsBalance({restaurantId : details.restaurant_id, marvels_balance: totalMarvels}, function(era, resu){
												if(era != null){
													console.log("Error : "+era);
													return res.json({status:false,Error: era});
												}else{
													console.log("marvels balance updated !");
													var newTransactionDetails = {
														restaurant_id: details.restaurant_id,
														amount: details.quantity,
														debited: true,
														transaction_time: details.transaction_time
													};
													marvolezApp.transaction(newTransactionDetails, function(err, result){
														if(err != null){
															console.log("Error : "+err);
															return res.json({status:false,Error: err});
														}else{
															marvolezApp.getRestaurantDeals(details.restaurant_id, function(erad, resad){
																if(erad != null){
																	console.log("Error : "+erad);
																	return res.json({status:false,Error: erad});
																}else{
																	var restaurantDeal = resad;
																	restaurantDeal.push(details);
																	var info = {
																		restaurantId : details.restaurant_id,
																		deal : restaurantDeal
																	};
																	
																	marvolezApp.updateRestaurantDeals(info, function(error_param, response_param){
																			if(error_param != null){
																				console.log("Error : "+err);
																				return res.json({status:false,Error: error_param});
																			}else{
																				console.log("successfully created deal");
																				return res.json({status:true,Message: "deal created successfully"});
																			}
																	});
																}
															});
														}
													});
												}
											});
										}
									}, true);
								}
							}
						});
					}else{
						return res.json({status:false,Message: "some empty field"});
					}
				}else{
					return res.json({status:false, Message: "some fields missing"});
				}
			}catch(e){
				console.log("exception caught");
				return res.json({status:false, Error: e});
			}
		});

		app.post('/createSecret', function(req, res){
			/*
			var secret = req.headers.secret;
			jwt.verify(secret, config.jwt.secret, function(err, decoded){
				if (err) {
			        return res.json({ success: false, message: 'Failed to authenticate token.' });   
			    } else {
			    	return res.json(decoded);
			    }
			});
			*/
			try{
				if(req.headers.hasOwnProperty('token')){
					var token = req.headers.token;
					jwt.verify(token, config.jwt.secret, function(err, decoded) {
						if (err) {
					        return res.json({ status: false, message: 'Failed to authenticate token.' });    
					    } else {
					    	req.decoded = decoded;
					        console.log(req.decoded);   
					        //return res.json(decoded)
					        var userid = null;
							if(req.decoded.hasOwnProperty("_id")){
								var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
								if(!checkForHexRegExp.test(req.decoded._id)){
									console.log('invalid userid');
									return res.json({ status: false, message: 'invalid userid' });
								}else{
									userid = new ObjectID(req.decoded._id);
									console.log('userid : '+userid);
									if(req.body.hasOwnProperty("restaurant_id") && req.body.hasOwnProperty("deal_id")){
										var d = new Date();
										var input = req.body;
										if(input.restaurant_id == "" || input.restaurant_id == null || input.deal_id == "" || input.deal_id == null){
											return res.json({ status: false, Error: 'restaurant_id or deal_id is blank' });
										}else{
											var details = {
												restaurant_id: new ObjectID(input.restaurant_id),
												deal_id: input.deal_id,
												quantity: 1,
												transaction_time: d.toString(),
												userid : userid
											};
											var token = self.jwtSign(details);
											return res.json({ status: true, secret: token });
										}
									}else{
										return res.json({ status: false, Error: 'restaurant_id or deal_id missing' });
									}
								}
							}else{
								console.log('no userid');
								return res.json({ status: false, message: 'no userid' });
							}
					    }
					});
				}else{
					console.log('token not sent');
					return res.json({status:false, Error: 'no auth token'});
				}
			}catch(e){
				console.log('some error occured :' + e);
				return res.json({status:false, Error: e});
			}
		});

		app.post('/redeemDeal', function(req, res){
			try{
				if(req.headers.hasOwnProperty('token')){
					var token = req.headers.token;
					jwt.verify(token, config.jwt.secret, function(err, decoded) {
						if (err) {
					        return res.json({ status: false, message: 'Failed to authenticate token.' });    
					    } else {
					        // if everything is good, save to request for use in other routes
					        req.decoded = decoded;
					        console.log(req.decoded);   
					        //return res.json(decoded)
					        var userid = null;
							if(req.decoded.hasOwnProperty("_id")){
								var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
								if(!checkForHexRegExp.test(req.decoded._id)){
									console.log('invalid userid');
									return res.json({ status: false, message: 'invalid userid' });
								}else{
									userid = new ObjectID(req.decoded._id);
									console.log('userid : '+userid);
									if(req.body.hasOwnProperty("restaurant_id") && req.body.hasOwnProperty("deal_id")){
										var d = new Date();
										var input = req.body;
										var details = {
											restaurant_id: new ObjectID(input.restaurant_id),
											deal_id: input.deal_id,
											quantity: 1,
											transaction_time: d.toString(),
											userid : userid
										};
										
										marvolezApp.redemptionHistory(details, function(errNo, sucYes){
											if(errNo != null){
												console.log("Error : "+errNo);
												return res.json({status:false,Error: errNo});
											}else{
												if(sucYes == 0){
													console.log('redeeming for first time');
													marvolezApp.getRestaurantDeals(details.restaurant_id, function(erad, resad){
														if(erad != null){
															console.log("Error : "+erad);
															return res.json({status:false,Error: erad});
														}else{
															var restaurantDeal = resad;
															if(!restaurantDeal.length){
																return res.json({status:false,Error: "Deals not available for given restaurant"});
															}else{
																var deal = self.getDeal(restaurantDeal, details.deal_id);
																if(deal == -1){
																	return res.json({status:false,Error: "Deal not found for given restaurant"});
																}else{
																	console.log(deal);
																	if(!deal.isActive){
																		return res.json({status:false,Error: "Deal expired"});
																	}else{
																		console.log(new Date().getTime());
																		console.log(new Date(deal.end_time).getTime());
																		if(new Date().getTime() < new Date(deal.end_time).getTime()){
																			return res.json({status:false,Error: "Deal expired"});
																		}else{
																			var dealQty = parseInt(deal.quantity);
																			if(dealQty <= 0){
																				return res.json({status:false,Error: "No Marvels left for redemption"});
																			}else{
																				var newDeal = {
																					quantity : dealQty - 1,
																					restaurant_id : deal.restaurant_id,
																					deal_details : deal.deal_details,
																					start_time : deal.start_time,
																					end_time : deal.end_time,
																					transaction_time : deal.transaction_time,
																					isActive : deal.isActive,
																					_id : deal._id,
																					deal_id : deal.deal_id,
																					last_transaction_time : d.toString()
																				};
																				var newDealArr = self.updateDeals(restaurantDeal, newDeal);
																				var info = {
																					restaurantId : details.restaurant_id,
																					deal : newDealArr
																				};
																				
																				marvolezApp.updateRestaurantDeals(info, function(error_param, response_param){
																						if(error_param != null){
																							console.log("Error : "+err);
																							return res.json({status:false,Error: error_param});
																						}else{
																							console.log("successful redemption");
																							marvolezApp.redeem(details, function(errar, resalt){
																								if(errar != null){
																									console.log('db error');
																									return res.json({ status: false, message: 'some error in db connectivity' });
																								}else{
																									console.log('inserted in redemptionHistory');
																									return res.json({status:true,Message: "Redemption Success"});
																								}
																							});
																							
																						}
																				});
																			}
																		}
																	}
																}
															}
														}
													});
												}else{
													return res.json({status:false,Error: "Already Redeemed for this user"});
												}
											}
										});
										
										/*
										
										*/
										
									}
								}
							}else{
								console.log('no userid');
								return res.json({ status: false, message: 'no userid' });
							}
					    }
					});
				}else{
					console.log('token not sent');
					return res.json({status:false, Error: 'no auth token'});
				}
			}catch(e){
				console.log('some error occured :' + e);
				return res.json({status:false, Error: e});
			}
		});


		app.post('/getPostData', function(req, res){
			try{
				if(req.headers.hasOwnProperty('token')){
					var token = req.headers.token;
					jwt.verify(token, config.jwt.secret, function(err, decoded) {
						if (err) {
							return res.json({ status: false, message: 'Failed to authenticate token.' });    
						} else {
							marvolezApp.GetRestaurantData(new ObjectID(req.body.restaurant_id), function(err6, result6){
								if(err6 != null){

								}else{
									console.log(result6);
									res.json(result6);
								}
							});
						}
					});
				}
			}catch(e){
				console.log('some error occured :' + e);
				return res.json({status:false, Error: e});
			}
		})

		app.post('/claimPost', function(req, res){
			try{
				if(req.headers.hasOwnProperty('token')){
					var token = req.headers.token;
					jwt.verify(token, config.jwt.secret, function(err, decoded) {
						if (err) {
					        return res.json({ status: false, message: 'Failed to authenticate token.' });    
					    } else {
					        // if everything is good, save to request for use in other routes
					        req.decoded = decoded;
					        console.log(req.decoded);   
					        //return res.json(decoded)
					        var userid = null;
							if(req.decoded.hasOwnProperty("_id")){
								var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
								if(!checkForHexRegExp.test(req.decoded._id)){
									console.log('invalid userid');
									return res.json({ status: false, message: 'invalid userid' });
								}else{
									userid = new ObjectID(req.decoded._id);
									console.log('userid : '+userid);

									var json = req.body;

									if(json.hasOwnProperty("restaurant_id") && json.restaurant_id != null){
										var detailsObject = {
											restaurant_id: new ObjectID(req.body.restaurant_id),
											restaurant_name: req.body.restaurant_name,
											location: req.body.location,
										}
										marvolezApp.getCreditsBalance(detailsObject.restaurant_id, function(err1, result1){
											if(err1 != null){

											}else{
												var restaurant_credits = result1;
												if(restaurant_credits < 2.0){
													console.log('Insufficient balance at restaurant');
													return res.json({ status: false, message: 'Insufficient balance at restaurant' });
												}else{
													var newRestaurantCredits = restaurant_credits - 2;
													var restaurantObject = {
														restaurantId : detailsObject.restaurant_id,
														restaurant_name : req.body.restaurant_name,
														location : req.body.location,
														Campaign_Credits : newRestaurantCredits,
														isFacebook:req.body.isFacebook,
														isTwitter:req.body.isTwitter,
													};

													if(newRestaurantCredits < 2.0){
														restaurantObject.adSharing = false;
													}

								

													marvolezApp.getMarvolezBalance(function(err2, result2){
														if(err2 != null){

														}else{
															var marvolez_credits = result2.marvolez_credits;
															marvolez_credits = marvolez_credits + 0.05;
															var marvolezObject = {
																_id : new ObjectID(result2._id),
																marvolez_credits : marvolez_credits,
																claimtype : detailsObject.claimtype
															}
															
															userApp.getUserDetails({_id : userid}, function(er, re){
																if(er != null){
																	console.log('token invalid/expired');
																	return res.json({status:false, Error: 'token invalid/expired'});
																}else{
																	var userAddCredits = 0;
																	if(re.hasOwnProperty("add_credits")){
																		userAddCredits = re.add_credits;
																	}
																	userAddCredits = userAddCredits + 0.50;
																	var userObject = {
																		_id : userid,
																		add_credits : userAddCredits,
																		claimtype : detailsObject.claimtype
																	};
																	//console.log(marvolezObject);
																	//console.log(restaurantObject);
																	var d = new Date();
																	var transactionObject = {
																		restaurant_id : restaurantObject.restaurantId,
																		restaurant_name : req.body.restaurant_name,
																		location : req.body.location,
																		user_id : userObject._id,
																		add_credits : 2.00,
																		isFacebook:req.body.isFacebook,
																		isTwitter:req.body.isTwitter,
																		status : "claimed",
																		reach: req.body.reach,
																		transaction_year: d.getFullYear(),
																		transaction_month: 1+d.getMonth(),
																		transaction_day: d.getDate(),
																	}
																	console.log(transactionObject);
																	marvolezApp.updateCreditsBalance(restaurantObject, function(err3, result3){
																		if(err3 != null){

																		}else{
																			marvolezApp.updateMarvolezBalance(marvolezObject, function(err4, result4){
																				if(err4 != null){

																				}else{
																					userApp.updateUserCredits(userObject, function(err5, result5){
																						if(err5 != null){

																						}else{
																							marvolezApp.claimTransaction(transactionObject, function(err6, result6){
																								if(err6 != null){

																								}else{
																									console.log("done");
																									return res.json({ status: true, message: 'transaction successfull' });
																								}
																							});
																						}
																					})
																				}
																			});
																		}
																	});
																}
															});
														}
													});
												}
											}
										});
									}else{
										console.log('no restaurant_id');
										return res.json({ status: false, message: 'no restaurant_id' });
									}
									/*
									marvolezApp.getMarvolezBalance(function(err, result){
										if(err != null){

										}else{
											console.log("at route : "+result._id);
										}
									});
									
									userApp.getUserDetails({_id : userid}, function(er, re){
										if(er != null){
											console.log('token invalid/expired');
											return res.json({status:false, Error: 'token invalid/expired'});
										}else{
											var userAddCredits = 0;
											if(re.hasOwnProperty("add_credits")){
												userAddCredits = re.add_credits;
											}
											console.log(userAddCredits)
										}
									});
									*/
								}
							}else{
								console.log('no userid');
								return res.json({ status: false, message: 'no userid' });
							}
					    }
					});
				}else{
					console.log('token not sent');
					return res.json({status:false, Error: 'no auth token'});
				}
			}catch(e){
				console.log('some error occured :' + e);
				return res.json({status:false, Error: e});
			}
		});

		app.post('/checkPastTransaction', function(req, res){
			try{
				if(req.headers.hasOwnProperty('token')){
					var token = req.headers.token;
					jwt.verify(token, config.jwt.secret, function(err, decoded) {
						if (err) {
					        return res.json({ status: false, message: 'Failed to authenticate token.' });    
					    } else {
					        // if everything is good, save to request for use in other routes
					        req.decoded = decoded;
					        console.log(req.decoded);   
					        //return res.json(decoded)
					        var userid = null;
							if(req.decoded.hasOwnProperty("_id")){
								var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
								if(!checkForHexRegExp.test(req.decoded._id)){
									console.log('invalid userid');
									return res.json({ status: false, message: 'invalid userid' });
								}else{
									userid = new ObjectID(req.decoded._id);
									console.log('userid : '+userid);

									var json = req.body;

									if(json.hasOwnProperty("restaurant_id") && json.restaurant_id != null){
										var detailsObject = {
											restaurant_id: new ObjectID(req.body.restaurant_id),
											user_id : userid
										}
										marvolezApp.getPastTransactions(detailsObject, function(err1, result1){
											if(err1 != null){
												if(result1 == "notransactions")
													return res.json({ status: true, message: 'no transactions available' });
												else
													return res.json({ status: false, message: 'some error' });
											}else{
												return res.json({status:false, message: "previous transaction available", transactions : result1});
											}
										});
									}else{
										console.log('no restaurant_id');
										return res.json({ status: false, message: 'no restaurant_id' });
									}
								}
							}else{
								console.log('no userid');
								return res.json({ status: false, message: 'no userid' });
							}
					    }
					});
				}else{
					console.log('token not sent');
					return res.json({status:false, Error: 'no auth token'});
				}
			}catch(e){
				console.log('some error occured :' + e);
				return res.json({status:false, Error: e});
			}
		});

		app.post('/getAllUserTransactions', function(req, res){
			try{
				if(req.headers.hasOwnProperty('token')){
					var token = req.headers.token;
					jwt.verify(token, config.jwt.secret, function(err, decoded) {
						if (err) {
					        return res.json({ status: false, message: 'Failed to authenticate token.' });    
					    } else {
					        // if everything is good, save to request for use in other routes
					        req.decoded = decoded;
					        console.log(req.decoded);   
					        //return res.json(decoded)
					        var userid = null;
							if(req.decoded.hasOwnProperty("_id")){
								var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
								if(!checkForHexRegExp.test(req.decoded._id)){
									console.log('invalid userid');
									return res.json({ status: false, message: 'invalid userid' });
								}else{
									userid = new ObjectID(req.decoded._id);
									console.log('userid : '+userid);
									marvolezApp.getAllPostTransactions(userid, function(err1, result1){
										if(err1 != null){
											if(result1 == "notransactions")
												return res.json({ status: false, message: 'no transactions available' });
											else
												return res.json({ status: false, message: 'some error' });
										}else{
											// marvolezApp.getRestaurantDetail()
											return res.json({status:true,Message: "successfully retrieved transactions", transactions : result1});
										}
									});
								}
							}else{
								console.log('no userid');
								return res.json({ status: false, message: 'no userid' });
							}
					    }
					});
				}else{
					console.log('token not sent');
					return res.json({status:false, Error: 'no auth token'});
				}
			}catch(e){
				console.log('some error occured :' + e);
				return res.json({status:false, Error: e});
			}
		});

		app.post('/getAllPaidUserTransactions', function(req, res){
			try{
				if(req.headers.hasOwnProperty('token')){
					var token = req.headers.token;
					jwt.verify(token, config.jwt.secret, function(err, decoded) {
						if (err) {
					        return res.json({ status: false, message: 'Failed to authenticate token.' });    
					    } else {
					        // if everything is good, save to request for use in other routes
					        req.decoded = decoded;
					        console.log(req.decoded);   
					        //return res.json(decoded)
					        var userid = null;
							if(req.decoded.hasOwnProperty("_id")){
								var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
								if(!checkForHexRegExp.test(req.decoded._id)){
									console.log('invalid userid');
									return res.json({ status: false, message: 'invalid userid' });
								}else{
									userid = new ObjectID(req.decoded._id);
									console.log('userid : '+userid);
									marvolezApp.getAllPostTransactions(userid, function(err1, result1){
										if(err1 != null){
											if(result1 == "notransactions")
												return res.json({ status: false, message: 'no transactions available' });
											else
												return res.json({ status: false, message: 'some error' });
										}else{
											return res.json({status:true,Message: "successfully retrieved transactions", transactions : result1});
										}
									}, true);
								}
							}else{
								console.log('no userid');
								return res.json({ status: false, message: 'no userid' });
							}
					    }
					});
				}else{
					console.log('token not sent');
					return res.json({status:false, Error: 'no auth token'});
				}
			}catch(e){
				console.log('some error occured :' + e);
				return res.json({status:false, Error: e});
			}
		});
		
		app.post('/getUserBalance', function(req, res){
			try{
				if(req.headers.hasOwnProperty('token')){
					var token = req.headers.token;
					jwt.verify(token, config.jwt.secret, function(err, decoded) {
						if (err) {
					        return res.json({ status: false, message: 'Failed to authenticate token.' });    
					    } else {
					        // if everything is good, save to request for use in other routes
					        req.decoded = decoded;
					        console.log(req.decoded);   
					        //return res.json(decoded)
					        var userid = null;
							if(req.decoded.hasOwnProperty("_id")){
								var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
								if(!checkForHexRegExp.test(req.decoded._id)){
									console.log('invalid userid');
									return res.json({ status: false, message: 'invalid userid' });
								}else{
									userid = new ObjectID(req.decoded._id);
									console.log('userid : '+userid);
									userApp.getUserDetails({_id : userid}, function(er, re){
										if(er != null){
											console.log('token invalid/expired');
											return res.json({status:false, Error: 'token invalid/expired'});
										}else{
											var userAddCredits = 0;
											if(re.hasOwnProperty("add_credits")){
												userAddCredits = re.add_credits;
											}
											return res.json({status:true, add_credits_balance: userAddCredits});
										}
									});
								}
							}else{
								console.log('no userid');
								return res.json({ status: false, message: 'no userid' });
							}
					    }
					});
				}else{
					console.log('token not sent');
					return res.json({status:false, Error: 'no auth token'});
				}
			}catch(e){
				console.log('some error occured :' + e);
				return res.json({status:false, Error: e});
			}
		});

		app.post('/cancelDeal', function(req, res){
			try{
				if(req.body.hasOwnProperty("deal_id") && req.body.hasOwnProperty("restaurant_id")){
					var input = req.body;
					if(input.deal_id != "" && input.deal_id != null && input.restaurant_id != "" && input.restaurant_id != null){

						var d = new Date();
						var details = {
							restaurant_id : new ObjectID(input.restaurant_id),
							deal_id: new ObjectID(input.deal_id),
							isActive : false,
							updationTime : d.toString()
						};
						console.log('retrieving restaurantDeals for : '+ details.restaurant_id);
						marvolezApp.getRestaurantDeals(details.restaurant_id, function(erad, resad){
							if(erad != null){
								console.log("Error : "+erad);
								return res.json({status:false,Error: erad});
							}else{
								var restaurantDeal = resad;
								if(!restaurantDeal.length){
									return res.json({status:false,Error: "No Deals not available for given restaurant"});
								}else{
									var deal = self.getDeal(restaurantDeal, details.deal_id);
									if(deal == -1){
										return res.json({status:false,Error: "Deal not found for given restaurant"});
									}else{
										if(!deal.isActive){
											return res.json({status:false,Error: "Deal Already Inactive"});
										}else{
											console.log("current time : " + new Date().getTime());
											console.log("deal end time : " + new Date(deal.end_time).getTime());
											if(new Date().getTime() < new Date(deal.end_time).getTime()){
												return res.json({status:false,Error: "Deal expired"});
											}else{
												var dealQty = parseInt(deal.quantity);
												if(dealQty <= 0){
													return res.json({status:false,Error: "No Marvels left invalid"});
												}else{
													var newDealArr = self.removeDeal(restaurantDeal, details.deal_id);
													var info = {
														restaurantId : details.restaurant_id,
														deal : newDealArr
													};
													
													var newTransactionDetails = {
														restaurant_id: details.restaurant_id,
														amount: dealQty,
														credited: true,
														transaction_time: d.toString()
													};

													marvolezApp.transaction(newTransactionDetails, function(err, result){
														if(err != null){
															console.log("Error : "+err);
															return res.json({status:false,Error: err});
														}else{
															console.log("transaction log successfull at "+d.toString());
															marvolezApp.getMarvelsBalance(newTransactionDetails.restaurant_id, function(e, re){
																if(e != null){
																	console.log("Error : "+e);
																	return res.json({status:false,Error: e});
																}else{
																	var totalMarvels = parseInt(newTransactionDetails.amount) + parseInt(re);
																	marvolezApp.updateMarvelsBalance({restaurantId : newTransactionDetails.restaurant_id, marvels_balance: totalMarvels}, function(era, resu){
																		if(era != null){
																			console.log("Error : "+era);
																			return res.json({status:false,Error: era});
																		}else{
																			console.log("marvels refunded successful at "+d.toString());
																			marvolezApp.updateRestaurantDeals(info, function(error_param, response_param){
																				if(error_param != null){
																					console.log("Error : "+err);
																					return res.json({status:false,Error: error_param});
																				}else{
																					console.log("restaurant deal updated !!!");
																					marvolezApp.transactDeal(details, function(ers, rest){
																						if(ers != null){
																							console.log("Error : "+ers);
																							return res.json({status:false,Error: ers});
																						}else{
																							return res.json({status:true,Message: "Deal cancelled successfully !!!"});
																						}
																					});
																				}
																			});
																		}
																	});
																}
															});
														}
													});

													/*
													return res.json({status:false,Error: newTransactionDetails});
													
													marvolezApp.updateRestaurantDeals(info, function(error_param, response_param){
															if(error_param != null){
																console.log("Error : "+err);
																return res.json({status:false,Error: error_param});
															}else{
					
															}
													});
													*/
												}
											}
										}
									}
								}
							}
						});
						/*
						marvolezApp.trasactDeal(details, function(er, re){

						});
						*/
					}else{
						return res.json({status:false,Message: "deal_id or restaurant_id is empty"});
					}
				}else{
					console.log("deal_id or restaurant_id is missing");
					return res.json({status: false, Error : "deal_id or restaurant_id missing"});
				}
			}catch(e){
				console.log("exception caught at cancelDeal");
				return res.json({status: false, Error : e});
			}
		});

		app.get('/testScheduler', function(req, res){
			var d = new Date();
			console.log('at test scheduler : '+ d.toString());
		});
		/*
		app.post('/transferMarvs', function(req, res){
			try{
				if(req.body.hasOwnProperty("restaurant_id") && req.body.hasOwnProperty("amount") && req.body.hasOwnProperty("credited_to")){
					var d = new Date();

					var newTransactionDetails = {
						restaurant_id: new ObjectID(req.body.restaurant_id),
						amount: req.body.amount,
						debited: true,
						debited_to: new ObjectID(req.body.credited_to),
						transaction_time: d.toString()
					};

					var OlezObj = {
						restaurant_id: newTransactionDetails.restaurant_id,
						user_id: newTransactionDetails.credited_to,
						olez: newTransactionDetails.amount,
						transaction_time: newTransactionDetails.transaction_time
					}
					marvolezApp.getMarvsBalance(newTransactionDetails.restaurant_id, function(e, r){
						if(e != null){
							console.log("Error : "+e);
							res.send("false");
						}else{
							if(parseInt(r) < parseInt(newTransactionDetails.amount)){
								console.log("Error : Insufficient Balance");
								res.send("false");
							}else{
								console.log("Marvs Balance is : "+r);
								var totalMarvs = parseInt(r) - parseInt(newTransactionDetails.amount) ;
								marvolezApp.updateMarvsBalance({restaurantId : newTransactionDetails.restaurant_id, marvs_balance: totalMarvs}, function(era, resu){
									if(era != null){
										console.log("Error : "+era);
										res.send("false");
									}else{
										console.log("marvs deducted successfully at "+d.toString());
										marvolezApp.getOlezBalance(OlezObj, function(err, count){
											if(err != null){
												console.log("Error : "+err);
												res.send("false");
											}else{
												var isFirst = -1;
												if(count == "resultNull"){
													count = 0;
													isFirst = 1;
												}
												var totalOlez = parseInt(OlezObj.olez) + parseInt(count);
												var OlezUpdate = {
													restaurant_id: newTransactionDetails.restaurant_id,
													user_id: newTransactionDetails.debited_to,
													olez_balance: totalOlez,
													last_updated: newTransactionDetails.transaction_time
												}
												if(isFirst == 1){
													marvolezApp.olezInsert(OlezUpdate, function(er, rest){
														if(er != null){
															console.log("Error : "+er);
															res.send("false");
														}else{
															console.log("Olez balance added successfully at "+d.toString());
															marvolezApp.transaction(newTransactionDetails, function(err, result){
																if(err != null){
																	console.log("Error : "+err);
																	res.send("false");
																}else{
																	console.log("Marvs transaction successfull at "+d.toString());
																	marvolezApp.transaction(OlezObj, function(ero, resu){
																		if(ero != null){
																			console.log("Error : "+ero);
																			res.send("false");
																		}else{
																			console.log("Olez transaction successfull at "+d.toString());
																			res.send("true");
																		}
																	}, true);
																}
															});
														}
													});
												}else{
													marvolezApp.updateOlezBalance(OlezUpdate, function(er, rest){
														if(er != null){
															console.log("Error : "+er);
															res.send("false");
														}else{
															console.log("Olez balance updated successfully at "+d.toString());
															marvolezApp.transaction(newTransactionDetails, function(err, result){
																if(err != null){
																	console.log("Error : "+err);
																	res.send("false");
																}else{
																	console.log("Marvs transaction successfull at "+d.toString());
																	marvolezApp.transaction(OlezObj, function(ero, resu){
																		if(ero != null){
																			console.log("Error : "+ero);
																			res.send("false");
																		}else{
																			console.log("Olez transaction successfull at "+d.toString());
																			res.send("true");
																		}
																	}, true);
																}
															});
														}
													});
												}
											}
										});
									}
								});
							}
						}
					});
					
					/*
					marvolezApp.transaction(newTransactionDetails, function(err, result){
						if(err != null){
							console.log("Error : "+err);
							res.send("false");
						}else{
							console.log("Marvs transaction successfull at "+d.toString());
							marvolezApp.transaction(OlezObj, function(er, resu){
								if(er != null){
									console.log("Error : "+er);
									res.send("false");
								}else{
									console.log("Olez transaction successfull at "+d.toString());
									res.send("true");
								}
							}, true);
						}
					});
					
				}else{
					console.log("some empty field");
					res.send("false");
				}
			}catch(er){
				console.log("error occured : "+er);
				res.send("false");
			}
		});
		*/
	}
}