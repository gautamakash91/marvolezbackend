
var stripe = require('stripe')('sk_live_2AHlFJZioRMYmSBUYIKaOs9m');
var hash = require('hash.js');

module.exports = {
	getHash : function(stringVar){
        return hash.sha256().update(stringVar).digest('hex');
    },
	configure: function (app, mongo, url, assert, ObjectID) {
		var self = this;

		app.post('/stripeCharge', function(req, res){
			var token  = req.body.stripeToken;
			var chargeAmount = req.body.chargeAmount;
			stripe.charges.create({
				amount : parseInt(chargeAmount),
				currency : "usd",
				source : token,
				description: "Charge for marvolez wallet"
			}, function(err, charge){
				if(err){
					return res.json({status:false, Message : err});
				}else{
					var d = new Date();
					var paymentdetails = {
						transaction_id: charge.id,
						restaurant_id:req.body.restaurant_id,
						ownerHash : self.getHash(req.body.email),
						transaction_year: d.getFullYear(),
						transaction_month: d.getMonth(),
						transaction_day: d.getDate(),
						Amount: parseInt(req.body.chargeAmount)/100
					}
					
					mongo.connect(url, function(err, db){
						assert.equal(null, err);
						db.collection('Payment_Audit').insertOne(paymentdetails,function(err, result){
						  if(err){
								res.send({status:false});
						  }else{
							db.collection("Restaurants").updateOne({"_id": new ObjectID(req.body.restaurant_id)}, { $inc: {Wallet: parseInt(paymentdetails.Amount) } }, {upsert: false}, function(err, result){							
								if(err){
									  res.send({status:false});
								}else{
									
									  res.send({status:true});
								}
							});
						  }
						});
					});
				}
			});
		});

		app.post('/stripeCharge2', function(req, res){
					var d = new Date();
					var paymentdetails = {
						restaurant_id:req.body.restaurant_id,
						ownerHash : self.getHash(req.body.email),
						transaction_year: d.getFullYear(),
						transaction_month: d.getMonth(),
						transaction_day: d.getDate(),
						Amount: parseInt(req.body.chargeAmount)/100
					}
					
					mongo.connect(url, function(err, db){
						assert.equal(null, err);
						db.collection('Payment_Audit').insertOne(paymentdetails,function(err, result){
						  if(err){
								res.send({status:false});
						  }else{
							db.collection("Restaurants").updateOne({"_id": new ObjectID(req.body.restaurant_id)}, { $inc: {Wallet: parseInt(paymentdetails.Amount) } }, {upsert: false}, function(err, result){							
								if(err){
									  res.send({status:false});
								}else{
									
									  res.send({status:true});
								}
							});
						  }
						});
					});
	});

		app.post('/stripeTransfer', function(req, res){
			stripe.transfers.create({
				amount: parseInt(req.body.amount),
				currency: "usd",
				destination: req.body.account,
			  }, function(err, transfer) {
				if(err){
					res.json({status:false, message: err});
				}else{
					res.json({status:true});
				}
			  });
		});

		app.post('/stripeBalance', function(req, res){
			stripe.balance.retrieve(function(err, balance) {
				res.json(balance);
			  });
		});

		
	}
}